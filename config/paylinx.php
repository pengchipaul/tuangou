<?php

return [

    /*
    | PAYLINX APPID AND APPKEY
    */

    'store_id' => env('PAYLINX_STOREID'),
    'mch_id' => env('PAYLINX_MCHID'),
    'app_key' => env('PAYLINX_APPKEY'),
    'alipay_mch_id' => env('ALIPAY_MCHID'),
    'verify_key' =>env('PAYLINX_VERIFY_KEY')

];
