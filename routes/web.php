<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('users.products.index'));
})->name('index');

Auth::routes();

/**
 * admin and staff login
 */

Route::namespace('Admin')->middleware('guest')->prefix('staff')->group(function(){
    Route::get('login','UserController@login');
});

/**
 * Routes for admin
 */

Route::middleware(['admin','language'])->namespace('Admin')->prefix('admin')->group(function(){

    /*
     * admin index route
     */
    Route::get('/','ProductController@index');

    /*
     * user related
     */
    Route::prefix('users')->group(function(){
        Route::get('all','UserController@index')->name('admin.users.index');
        Route::get('{id}/show','UserController@show')->name('admin.users.show');
        Route::get('create','UserController@create')->name('admin.users.create');
        Route::post('store','UserController@store')->name('admin.users.store');
        Route::get('{id}/edit','UserController@edit')->name('admin.users.edit');
        Route::patch('{id}/update','UserController@update')->name('admin.users.update');
        Route::delete('{id}/delete','UserController@delete')->name('admin.users.delete');

        Route::get('staff_all','UserController@staff_index')->name('admin.users.staff_index');
    });

    /* Product related */
    Route::prefix('products')->group(function (){
        Route::get('all','ProductController@index')->name('admin.products.index');
        Route::get('{id}/show','ProductController@show')->name('admin.products.show');
        Route::get('create','ProductController@create')->name('admin.products.create');
        Route::post('store','ProductController@store')->name('admin.products.store');
        Route::get('{id}/edit','ProductController@edit')->name('admin.products.edit');
        Route::patch('{id}/update','ProductController@update')->name('admin.products.update');
        Route::delete('{id}/delete','ProductController@delete')->name('admin.products.delete');
        Route::get('trashed','ProductController@trashed')->name('admin.products.trashed');
        Route::post('{id}/restore','ProductController@restore')->name('admin.products.restore');
    });

    /* Banner related */
    Route::prefix('banner')->group(function(){
        Route::get('all','BannerController@index')->name('admin.banner.index');
        Route::get('{id}/show','BannerController@show')->name('admin.banner.show');
        Route::get('create','BannerController@create')->name('admin.banner.create');
        Route::post('store','BannerController@store')->name('admin.banner.store');
        Route::get('{id}/edit','BannerController@edit')->name('admin.banner.edit');
        Route::patch('{id}/update','BannerController@update')->name('admin.banner.update');
        Route::delete('{id}/delete','BannerController@delete')->name('admin.banner.delete');
    });

    /* Category related */
    Route::prefix('categories')->group(function(){
        Route::get('all','CategoryController@index')->name('admin.categories.index');
        Route::get('{id}/show','CategoryController@show')->name('admin.categories.show');
        Route::get('create','CategoryController@create')->name('admin.categories.create');
        Route::post('store','CategoryController@store')->name('admin.categories.store');
        Route::get('{id}/edit','CategoryController@edit')->name('admin.categories.edit');
        Route::patch('{id}/update','CategoryController@update')->name('admin.categories.update');
        Route::delete('{id}/delete','CategoryController@delete')->name('admin.categories.delete');
        Route::get('trashed','CategoryController@trashed')->name('admin.categories.trashed');
        Route::post('{id}/restore','CategoryController@restore')->name('admin.categories.restore');
    });

    /* Subcategory related */
    Route::prefix('subcategories')->group(function(){
        Route::get('all','SubcategoryController@index')->name('admin.subcategories.index');
        Route::get('{id}/show','SubcategoryController@show')->name('admin.subcategories.show');
        Route::get('create','SubcategoryController@create')->name('admin.subcategories.create');
        Route::post('store','SubcategoryController@store')->name('admin.subcategories.store');
        Route::get('{id}/edit','SubcategoryController@edit')->name('admin.subcategories.edit');
        Route::patch('{id}/update','SubcategoryController@update')->name('admin.subcategories.update');
        Route::delete('{id}/delete','SubcategoryController@delete')->name('admin.subcategories.delete');
    });

    /* Delivery Location related */
    Route::prefix('locations')->group(function(){
        Route::get('all','LocationController@index')->name('admin.locations.index');
        Route::get('{id}/show','LocationController@show')->name('admin.locations.show');
        Route::get('create','LocationController@create')->name('admin.locations.create');
        Route::post('store','LocationController@store')->name('admin.locations.store');
        Route::get('{id}/edit','LocationController@edit')->name('admin.locations.edit');
        Route::patch('{id}/update','LocationController@update')->name('admin.locations.update');
        Route::delete('{id}/delete','LocationController@delete')->name('admin.locations.delete');
        Route::post('{location_id}/store_pickup','PickupTimeController@store')->name('admin.locations.store_pickup');
        Route::patch('{location_id}/update_pickup','PickupTimeController@update')->name('admin.locations.update_pickup');
    });

    /* Order related */
    Route::prefix('orders')->group(function(){
        Route::get('all','OrderController@index')->name('admin.orders.index');
        Route::get('{id}/show','OrderController@show')->name('admin.orders.show');
        Route::get('show_search','OrderController@show_search')->name('admin.orders.show_search');
        Route::post('search','OrderController@search')->name('admin.orders.search');
        Route::get('all/paid','OrderController@paid')->name('admin.orders.paid');
        Route::get('all/unpaid','OrderController@unpaid')->name('admin.orders.unpaid');
        Route::patch('{id}/confirm','OrderController@confirm')->name('admin.orders.confirm');
        Route::post('release','OrderController@release_stock')->name('admin.orders.release');
    });

    /* Coupon related */
    Route::prefix('coupons')->group(function(){
        Route::get('all','CouponController@index')->name('admin.coupons.index');
        Route::get('export','CouponController@export')->name('admin.coupons.export');
        Route::get('show_search','CouponController@show_search')->name('admin.coupons.show_search');
        Route::post('search','CouponController@search')->name('admin.coupons.search');
        Route::post('{id}/mark','CouponController@mark')->name('admin.coupons.mark');
    });
});

/**
 * Routes for guests and users
 */
Route::namespace('Web')->middleware(['web','language'])->group(function(){
    Route::get('languages/{locale}','HomeController@change_language')->name('change_language');
    Route::get('home','HomeController@index')->name('home');

    /* functions for mall users */
    Route::prefix('mall')->group(function(){

        /* assets related */
        Route::get('assets/banner_products','HomeController@banner_products');

        /* home page */
        Route::get('enter/{lang}','HomeController@enter')->name('home.enter');

        /* products related */
        Route::prefix('products')->group(function(){
            Route::get('/','ProductController@index')->name('users.products.index');
            Route::get('get_products','ProductController@get_products')->name('users.products.get_products');
            Route::get('get_product/{id}','ProductController@get_product');
        });

        /* categories related */
        Route::prefix('categories')->group(function(){
            Route::get('get/{id}','CategoryController@get_products')->name('users.categories.get_products');
        });

        /* subcategories related */
        Route::prefix('subcategories')->group(function(){
            Route::get('get/{id}','CategoryController@get_products')->name('users.subcategories.get_products');
        });

        /* cart related */
        Route::prefix('cart')->group(function(){
            Route::get('show','CartController@show')->name('users.show_cart');
            Route::get('all','CartController@get_cart')->name('users.get_cart');
            Route::post('add','CartController@add_to_cart')->name('users.add_to_cart');
            Route::post('minus','CartController@minus_from_cart')->name('users.minus_from_cart');
            Route::post('update_cart_item','CartController@update_cart_item')->name('users.update_cart_item');
            Route::post('submit','CartController@submit_cart')->name('users.submit_cart');
            Route::delete('delete/{id}','CartController@delete_cart_item')->name('users.delete_cart_item');
            Route::delete('clear','CartController@clear_cart')->name('users.clear_cart');
        });

        /* order related */
        Route::prefix('order')->middleware('make_order')->group(function() {
            Route::get('form', 'OrderController@show_form')->name('users.orders.show_form');
            Route::post('submit', 'OrderController@submit')->name('users.orders.submit');
            Route::get('confirm', 'OrderController@confirm')->name('users.orders.confirm')->middleware('complete_order_info');
            Route::post('pay', 'OrderController@pay')->name('users.orders.pay')->middleware('complete_order_info');
        });
    });
});

/**
 * Routes for verifying payments
 */
Route::namespace('Web')->prefix('payment_verify')->group(function(){
    Route::post(config('paylinx.verify_key').'/verify','PaymentController@verify');
    Route::get('result','OrderController@payment_result')->name('users.orders.result')->middleware('language');
});

/**
 * Routes for testing purpose
 */
Route::namespace('Test')->prefix('test')->group(function(){
    Route::get('page','ViewController@page');
});