import Vue from 'vue';
import VueInternationalization from 'vue-i18n';
import Locale from './vue-i18n-locales.generated';

/* swipe lib */
import { Swipe, SwipeItem } from 'vue-swipe';

Vue.use(VueInternationalization);

const lang = document.documentElement.lang.substr(0, 2);

const i18n = new VueInternationalization({
    locale: lang,
    messages: Locale
});

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * CSS library
 */
require('vue-swipe/dist/vue-swipe.css');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('products', require('./components/Products'));
Vue.component('cart', require('./components/Cart'));

/* components for large screen */
Vue.component('products-lg',require('./components/mobile_large/Products'));
Vue.component('cart-lg',require('./components/mobile_large/Cart'));
Vue.component('location-lg',require('./components/mobile_large/Widget/Location'));

/**
 * Other component
 */
Vue.component('swipe',Swipe);
Vue.component('swipe-item',SwipeItem);

/* register widgets */
Vue.component('location',require('./components/Widget/Locatioin'));
Vue.component('slider',require('./components/Widget/Slider'));

const app = new Vue({
    el: '#app',
    i18n,
});
