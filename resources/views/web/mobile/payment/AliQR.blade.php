@extends('layouts.app')
@section('content')
    <a href="{{route('users.products.index')}}">
        <div><img id="top-logo" class="w-100" src="{{asset('public/assets/w100_LOGO.png')}}"></div>
    </a>    <div class="container mt-5 mb-5 ">
        <h3 class="text-dark text-center">信息确认</h3>
    </div>

    <div class="success_img">
        <img class="w-100" src="{{ $pic_url}}">
    </div>

    <div class="container-fluid text-center fixed-bottom pb-5">
        <h5>请保存图片，扫码支付</h5>
    </div>

    <a href="{{route('users.products.index')}}" class="text-light">
        <div class="w-100 btn btn-block btn-pink text-white red no-border" style="position: fixed; bottom: 0px">
            重新开始
        </div>
    </a>

@endsection