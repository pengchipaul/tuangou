@extends('layouts.app')

@section('content')
    <a href="{{route('users.products.index')}}">
        <div><img id="top-logo" class="w-100" src="{{asset('public/assets/w100_LOGO.png')}}"></div>
    </a>
    <div id="info-page" class="container-fluid mb-5">
        <div class="container-fluid mt-4 text-light">

            <div class="container mb-4">
                <h3 class="text-dark text-center">{{__('信息填写')}}</h3>
            </div>
            <form method="POST" action="{{route('users.orders.submit')}}">
                @csrf
                <div class="form-group row">
                    <label for="phone" class="col-4 col-form-label pr-1 pl-0 text-dark text-right"
                           style="font-size: medium; "><span class="text-danger">*</span>{{__('手机号')}}:</label>

                    <div class="col-8">
                        <input id="phone" type="number"
                               class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" required
                               autofocus @if(isset($order_info)) value="{{$order_info['phone']}}" @else value="{{ old('phone') }}" @endif >

                        @if ($errors->has('phone'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="name" class="col-4 col-form-label pr-1 pl-0 text-dark text-right"
                           style="font-size: medium"><span class="text-danger">*</span>{{__('姓名')}}:</label>

                    <div class="col-8">
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                               name="name" required autofocus @if(isset($order_info)) value="{{$order_info['name']}}"  @else value="{{ old('name') }}" @endif >

                        @if ($errors->has('name'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="coupon" class="col-4 col-form-label pr-1 pl-0 text-dark text-right"
                           style="font-size: medium">{{__('代金券')}}:</label>

                    <div class="col-8">
                        <input id="coupon" type="text"
                               class="form-control{{ $errors->has('coupon') ? ' is-invalid' : '' }}"
                               name="coupon" autofocus @if(isset($order_info) && isset($order_info['coupon'])) value="{{$order_info['coupon']}}" @else value="{{ old('coupon') }}" @endif >

                        @if ($errors->has('coupon'))
                            <span class="invalid-feedback">
                                        <strong>{{ __($errors->first('coupon')) }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="payment_method" class="col-4 col-form-label pr-1 pl-0 text-dark text-right"
                           style="font-size: medium"><span class="text-danger">*</span>{{__('支付方式')}}:</label>

                    <div class="col-8">
                        <select id="payment_method" name="payment_method" class="form-control" required autofocus>
                            <option disabled selected>{{__('请选择支付方式')}}</option>
                            <option value="wechat" @if(old('payment_method') == "wechat") selected @endif>{{__('微信支付')}} </option>
                            <option value="alipay" @if(old('payment_method') == "alipay") selected @endif>{{__('支付宝支付')}} </option>
                            <option value="cod" @if(old('payment_method') == "cod") selected @endif>{{__('货到付款')}}</option>

                        </select>
                    </div>

                    @if ($errors->has('payment_method'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('payment_method') }}</strong>
                                    </span>
                    @endif

                </div>
                <Location :location-options="'{{json_encode($locations)}}'">
                    @if ($errors->has('location'))
                        <span class="invalid-feedback" style="display: block">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span>
                    @endif
                </Location>

                <div class="w-100 d-flex fixed-bottom">
                    <a href="{{route('users.show_cart')}}" class="btn btn-pink text-white w-100 btn-0-radius"><i
                                class="fas fa-shopping-cart"></i> {{__('返回购物车')}} </a>
                    <button type="submit" class="btn btn-danger w-100 btn-0-radius"><i class="far fa-file-alt"></i> {{__('提交订单')}}
                    </button>
                </div>

            </form>
        </div>


    </div>


@endsection