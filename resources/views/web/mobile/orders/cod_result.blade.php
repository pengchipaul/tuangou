@extends('layouts.app')

@section('content')

    <a href="{{route('users.products.index')}}">
        <div><img id="top-logo" class="w-100" src="{{asset('public/assets/w100_LOGO.png')}}"></div>
    </a>    <div class="container mt-5 mb-5 ">
        <h3 class="text-dark text-center">{{__('信息确认')}}</h3>
    </div>

    <div class="success_img">
        <img class="w-100" src="{{asset('public/assets/gift.png')}}">
        <h5 class="text-white " style="position: absolute; top: 118px; left: 40px"> {{__('下单成功')}}</h5>
    </div>




    <a href="{{route('home')}}" class="text-light">
        <div class="w-100 btn btn-block btn-pink text-white red no-border" style="position: fixed; bottom: 0px">
            {{__('确认')}}
        </div>
    </a>


@endsection