@extends('layouts.app')

@section('content')
    <a href="{{route('users.products.index')}}">
        <div><img id="top-logo" class="w-100" src="{{asset('public/assets/w100_LOGO.png')}}"></div>
    </a>
    <div class="mb-5 mt-3" style="border-radius: 5px; overflow: hidden;margin-left: 5%; width: 90%">

        <table class="w-100" style="font-size: small">
            <thead>
            <tr>
                <th colspan="2"
                    style="border-bottom: 0; padding: 3px 15px 3px 15px;max-width: none;font-size: 14px"
                    class="w-100 text-center bg-pink text-white">{{__('收货信息')}}
                </th>
            </tr>
            </thead>
            <tr>
                <td style="border-bottom: 1px dashed #a02e2a;background: white;padding: 5px 15px 5px 15px;word-wrap:break-word;font-weight: bold">
                    {{__('姓名')}}:
                </td>
                <td class="text-right"
                    style="border-bottom: 1px dashed #a02e2a;background: white;padding: 5px 15px 5px 15px;word-wrap:break-word;font-weight: bold">
                    {{__($order_info['name'])}}
                </td>
            </tr>
            <tr>
                <td style="border-bottom: 1px dashed #a02e2a;background: white;padding: 5px 15px 5px 15px;word-wrap:break-word;font-weight: bold">
                    {{__('电话')}}:
                </td>
                <td class="text-right"
                    style="border-bottom: 1px dashed #a02e2a;background: white;padding: 5px 15px 5px 15px;word-wrap:break-word;font-weight: bold">
                    {{__($order_info['phone'])}}
                </td>
            </tr>
            <tr>
                <td style="border-bottom: 1px dashed #a02e2a;background: white;padding: 5px 15px 5px 15px;word-wrap:break-word;font-weight: bold">
                    {{__('支付方式')}}:
                </td>
                <td class="text-right"
                    style="border-bottom: 1px dashed #a02e2a;background: white;padding: 5px 15px 5px 15px;word-wrap:break-word;font-weight: bold">
                    @if($order_info['payment_method'] == 'wechat')
                        {{__('微信支付')}}
                    @elseif($order_info['payment_method'] == 'alipay')
                        {{__('支付宝')}}
                    @elseif($order_info['payment_method'] == 'cod')
                        {{__('货到付款')}}
                    @endif
                </td>
            </tr>
            <tr>
                <td style="border-bottom: 1px dashed #a02e2a;background: white;padding: 5px 15px 5px 15px;word-wrap:break-word;font-weight: bold">
                    {{__('收货地址')}}:
                </td>
                <td class="text-right"
                    style="border-bottom: 1px dashed #a02e2a;background: white;padding: 5px 15px 5px 15px;word-wrap:break-word;font-weight: bold">
                    {{__($location->name)}}
                </td>
            </tr>
            <tr>
                <td colspan="2" style="background: white;padding: 5px 15px 5px 15px;font-weight: bold">
                    <span>
                        {{__($location->detail)}}
                    </span>
                    <br>
                    <span>
                        {{__($pickup_time->time)}}
                    </span>
                </td>
            </tr>

            @if(isset($order_info->coupon))
                <tr>
                    <td style="border-bottom: 1px dashed #a02e2a;background: white;padding: 5px 15px 5px 15px;word-wrap:break-word;font-weight: bold">
                        {{__('优惠码')}}: {{$order_info['coupon']}}
                    </td>
                </tr>
            @endif

        </table>

    </div>

    <div class="mb-5 mt-3" style="border-radius: 5px; overflow: hidden;margin-left: 5%; width: 90%">

        <table class="w-100" style="font-size: small">
            <colgroup>
                <col width="25%">
                <col width="25%">
                <col width="25%">
                <col width="25%">
            </colgroup>
            <thead>
            <tr>
                <th colspan=4
                    style="border-bottom: 0; padding: 3px 15px 3px 15px;max-width: none;font-size: 14px"
                    class="w-100 text-center bg-pink text-white">{{__('订单信息')}}
                </th>
            </tr>
            </thead>

            @foreach($products as $product)
                <tr>
                    <td colspan=3 style="background: white;padding: 5px 15px 5px 15px;word-wrap:break-word;">
                        <strong>{{__($product->name)}}</strong>
                        <p class="m-0 small">{{__('库存')}}:{{$product->stock}}</p>
                    </td>
                    <td colspan=1 style="background: white;padding: 5px 15px 5px 15px;word-wrap:break-word;">
                        <a href="{{asset('storage/app/'.$product->image)}}" target="_blank">
                            <img class="img-thumbnail mx-auto d-block" src="{{asset('storage/app/'.$product->image)}}">
                        </a>
                    </td>
                </tr>
                <tr>
                    <td colspan=2 class="text-pink"
                        style="background: white;padding: 5px 15px 5px 15px;word-wrap:break-word;font-weight: bold">
                        <span>{{__('数量')}}:{{$product->quantity}}</span>
                    </td>
                    <td colspan=2 class="text-right text-pink"
                        style="background: white;padding: 5px 15px 5px 15px;word-wrap:break-word;font-weight: bold">
                        {{__('单价')}}:${{$product->price}}</td>
                </tr>
                <tr>
                    <td colspan="4" class="text-right text-pink"
                        style="border-bottom: 1px solid #a02e2a;background: white;padding: 5px 15px 5px 15px;word-wrap:break-word;font-weight: bold">
                        {{__('小记')}}:${{$product->total}}</td>
                </tr>
            @endforeach

            @if(isset($order_info['discount']))
                <tr>
                    <td colspan="2"
                        style="border-bottom: 1px solid #a02e2a;background: white;padding: 5px 15px 5px 15px;word-wrap:break-word;font-size: 16px">
                        <strong>{{__('折扣')}}：</strong></td>
                    <td colspan="2" class="text-right"
                        style="border-bottom: 1px solid #a02e2a;background: white;padding: 5px 15px 5px 15px;word-wrap:break-word;color: #a02e2a;font-weight: bold;font-size: 16px;">
                        <strong>${{$order_info['discount']}}</strong></td>
                </tr>
            @endif
            <tr>
                <td colspan="2"
                    style="background: white;padding: 5px 15px 5px 15px;word-wrap:break-word;font-size: 16px">
                    <strong>{{__('总计')}}：</strong></td>
                <td colspan="2" class="text-right text-pink"
                    style="background: white;padding: 5px 15px 5px 15px;word-wrap:break-word;font-weight: bold;font-size: 16px;">
                    <strong>${{$order_info['total']}}</strong></td>
            </tr>
        </table>

    </div>

    <div class="w-100 d-flex fixed-bottom">
        <a href="{{route('users.orders.show_form')}}" class="btn btn-pink text-white w-100 btn-0-radius"><i
                    class="fas fa-undo-alt"></i> {{__('编辑订单')}} </a>
        <form id="submit-form" action="{{route('users.orders.pay')}}" method="post" class="w-100 mb-0">
            @csrf
            <button id="confirm-btn" type="submit" class="btn btn-danger w-100 btn-0-radius"><i
                        class="fas fa-dollar-sign"></i> {{__('确认并支付')}}
            </button>
        </form>
    </div>

@endsection
