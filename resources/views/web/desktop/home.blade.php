@extends('layouts.app')

@section('content')
    <div class="mb-0 mt-3" style="margin-left: 42.5%; width: 15%">
        <img class="w-100" src="{{asset('public/assets/med_logo.png')}}">
    </div>

    <div class="mb-0" style="margin-left: 42.5%; width: 15%">
        <img class="w-100" src="{{asset('public/assets/desktop.png')}}">
    </div>
    <div style="margin-left: 40%; width: 20%; color:#af1f24" class="text-center mb-5">
        Sorry!</br>
        This service only used on Mobile device temporarily!
    </div>
    <div style="margin-left: 40%; width: 20%; color:#af1f24" class="text-danger mb-5">
        <h5 class="text-center">OR</h5>
    </div>
    <div style="margin-left: 45%; width: 10%">
        <img class="w-100" src="{{asset('public/assets/FS_QRcode.jpeg')}}">
    </div>
    <div style="margin-left: 40%; width: 20%; color:#af1f24" class="text-center mb-5">
        Please scan this QR code on WeChat
    </div>


@endsection