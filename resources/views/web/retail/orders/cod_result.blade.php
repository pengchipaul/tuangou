@extends('layouts.app')

<style>
    #confirm-img {
        position: fixed;
        top: 50vh;
        left: 50vw;
        transform: translateX(-50%) translateY(-50%);
    }

    #confirm-text {
        font-size: 40px;
        position: absolute;
        left: 50%;
        bottom: 10px;
        transform: translateX(-50%);
    }

    div, a, button {
        font-size: 32px !important;
    }
</style>


@section('content')

    <a href="{{route('users.products.index')}}">
        <div><img id="top-logo" class="w-100" src="{{asset('public/assets/w100_LOGO.png')}}"></div>
    </a>
    <div class="w-100 red no-border container-fluid p-0 nav-btns pb-5">
        <a href="{{route('users.products.index')}}" class="btn btn-block btn-pink text-light">
            {{__('重新开始')}}
        </a>
    </div>
    <div class="container mt-5 mb-5">
        <h1 class="text-dark text-center" style="font-size: 60px">{{__('信息确认')}}</h1>
    </div>

    <div id="confirm-img">
        <img class="w-100" src="{{asset('public/assets/gift.png')}}">
        <h1 class="text-white" id="confirm-text"> {{__('下单成功')}}</h1>
    </div>






@endsection