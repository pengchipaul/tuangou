@extends('layouts.app')

<style>
    div, button, a {
        font-size: 32px !important;
    }

    .form-control {
        font-size: 32px !important;
        width: 70% !important;
    }

    select.form-control {
        height: 65px !important;
    }
</style>

@section('content')
    <a href="{{route('users.products.index')}}">
        <div><img id="top-logo" class="w-100" src="{{asset('public/assets/w100_LOGO.png')}}"></div>
    </a>
    <div id="info-page" class="container-fluid p-0 mb-5">
        <div class="container-fluid p-0 text-light">

            <form method="POST" action="{{route('users.orders.submit')}}">
                <div class="w-100 d-flex container-fluid p-0 nav-btns">
                    <a href="{{route('users.show_cart')}}" class="btn btn-pink text-white w-100 btn-0-radius"><i
                                class="fas fa-shopping-cart"></i> {{__('返回购物车')}} </a>
                    <button type="submit" class="btn btn-danger w-100 btn-0-radius"><i class="far fa-file-alt"></i> {{__('提交订单')}}
                    </button>
                </div>
                @csrf
                <div class="container mb-5">
                    <h1 class="text-dark text-center" style="font-size: 40px; margin-top: 100px">{{__('信息填写')}}</h1>
                </div>
                <div class="form-group row mb-5">
                    <label for="phone" class="col-4 col-form-label pr-1 pl-0 text-dark text-right">
                        <span class="text-danger">*</span>{{__('手机号')}}:</label>

                    <div class="col-8">
                        <input id="phone" type="number"
                               class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" required
                               autofocus @if(isset($order_info)) value="{{$order_info['phone']}}" @else value="{{ old('phone') }}" @endif >

                        @if ($errors->has('phone'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row mb-5">
                    <label for="name" class="col-4 col-form-label pr-1 pl-0 text-dark text-right">
                        <span class="text-danger">*</span>{{__('姓名')}}:</label>

                    <div class="col-8">
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                               name="name" required autofocus @if(isset($order_info)) value="{{$order_info['name']}}"  @else value="{{ old('name') }}" @endif >

                        @if ($errors->has('name'))
                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row mb-5">
                    <label for="payment_method" class="col-4 col-form-label pr-1 pl-0 text-dark text-right">
                        <span class="text-danger">*</span>{{__('支付方式')}}:</label>

                    <div class="col-8">
                        <select id="payment_method" name="payment_method" class="form-control" required autofocus>
                            <option disabled selected>{{__('请选择支付方式')}}</option>
                            <option value="wechat" @if(old('payment_method') == "wechat") selected @endif>{{__('微信支付')}} </option>
                            <option value="alipay" @if(old('payment_method') == "alipay") selected @endif>{{__('支付宝支付')}} </option>
                            <option value="cod" @if(old('payment_method') == "cod") selected @endif>{{__('货到付款')}}</option>

                        </select>
                    </div>

                    @if ($errors->has('payment_method'))
                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('payment_method') }}</strong>
                                    </span>
                    @endif

                </div>
                
                <Location-Lg :location-options="'{{json_encode($locations)}}'">
                    @if ($errors->has('location'))
                        <span class="invalid-feedback" style="display: block">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span>
                    @endif
                </Location-Lg>


            </form>
        </div>


    </div>

@endsection