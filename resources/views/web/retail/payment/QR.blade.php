@extends('layouts.app')

<style>
    #confirm-img {
        position: fixed;
        top: 50vh;
        left: 50vw;
        transform: translateX(-50%) translateY(-50%);
    }

    #confirm-text {
        font-size: 40px;
        position: absolute;
        left: 50%;
        bottom: 10px;
        transform: translateX(-50%);
    }

    div, a, button {
        font-size: 32px !important;
    }
</style>

@section('content')
    <a href="{{route('users.products.index')}}">
        <div><img id="top-logo" class="w-100" src="{{asset('public/assets/w100_LOGO.png')}}"></div>
    </a>
    <div class="w-100 red no-border container-fluid p-0 nav-btns" >
        <a href="{{route('users.products.index')}}" class="text-light btn btn-block btn-pink">
            重新开始
        </a>
    </div>
    <div class="container mt-5 mb-5 ">
        <h1 class="text-dark text-center" style="font-size: 60px">信息确认</h1>
    </div>

    <div id="confirm-img">
        <img class="w-100" src="{{ $pic_url}}">
        <h1 class="text-center mt-5">请扫描二维码支付</h1>
    </div>




@endsection