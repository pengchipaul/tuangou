<div class="row">
    <div class="col-md-6 offset-md-3 col-12 btn-group d-flex" role="group">
        @foreach($tabs as $tab)
            <a href="{{ $tab['link'] }}" class="btn btn-info w-100">{{$tab['label']}}</a>
        @endforeach
    </div>
</div>
