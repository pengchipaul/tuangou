<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ __('团多多') }}</title>

    <!-- JS libs -->
    <script src="{{ asset('public/js/custom.js') }}"></script>
    <script src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>


    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="{{ asset('public/css/fontawesome-all.css') }}" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/custom.css') }}" rel="stylesheet">

    @if(Auth::check() && Auth::user()->email == 'retail')
        <style>
            .fixed-bottom a {
                padding-top: 20px !important;
                padding-bottom: 20px !important;
            }

            .fixed-bottom button {
                padding-top: 20px !important;
                padding-bottom: 20px !important;
            }

            .nav-btns.container-fluid {
                border: white 2px solid;
            }

            .nav-btns.container-fluid button,a{
                padding-top: 20px !important;
                padding-bottom: 20px !important;
            }

        </style>
    @endif
</head>
<body>
<div id="app">
    @auth
        @if(Auth::user()->admin == 1)
            <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
                <div class="container">
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ __('团团乐 CrazyGet') }}
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false"
                            aria-label="{{ __('Toggle navigation') }}">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">
                        </ul>


                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">

                            @guest
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('登录') }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('注册') }}</a>
                                </li>
                            @endguest

                            <li class="nav-item dropdown">
                                <a id="langbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    语言/Language <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="langbarDropdown">
                                    <a class="dropdown-item"
                                       href="{{ route('change_language',['locale'=>'en']) }}">{{ __('English') }}</a>
                                    <a class="dropdown-item"
                                       href="{{ route('change_language',['locale'=>'cn']) }}">{{ __('中文') }}</a>
                                </div>
                            </li>


                            @if(Auth::check() && Auth::user()->admin == 1)
                            <!-- Authentication Links -->
                                {{-- navigation buttons for admin --}}
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('admin.banner.index') }}">{{ __('首页产品') }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('admin.coupons.index') }}">{{ __('折扣卷') }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('admin.orders.index') }}">{{ __('订单') }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('admin.locations.index') }}">{{ __('物流') }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('admin.products.index') }}">{{ __('产品') }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('admin.categories.index') }}">{{ __('类别') }}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('admin.users.index') }}">{{ __('用户') }}</a>
                                </li>
                            @endif
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        </ul>

                    </div>
                </div>
            </nav>
        @endif
    @endauth

    <main @if(!isset($full_screen)) class="py-4" @endif>
        @yield('content')
    </main>

</div>
<!-- Scripts -->
<script src="{{ asset('public/js/app.js') }}" defer></script>
</body>
</html>
