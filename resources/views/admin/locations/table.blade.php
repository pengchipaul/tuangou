<table class="table table-bordered table-hover text-center">
    <thead>
    @foreach($key_name as $key)
        <th>
            {{$key}}
        </th>
    @endforeach
    </thead>
    <tbody>
    @foreach($data_array as $data)
        <tr>
            @foreach($key_array as $key)
                <td>
                    {{-- Customization of data --}}
                    @if(strpos($key,'pickup_time') !== false)
                        @foreach($data->pickup_times as $pickup_time)
                            <div>{{$pickup_time->time}}</div>
                        @endforeach
                    @else
                        {{$data[$key]}}
                    @endif
                </td>
            @endforeach
            <td>
                <div class="btn-group-vertical">
                    @if(isset($show_link))
                        <a href="{{route($show_link,['id' => $data['id']])}}"
                           class="btn btn-info text-light">{{ __('显示')}}</a>
                    @endif
                    @if(isset($edit_link))
                        <a href="{{route($edit_link,['id' => $data['id']])}}"
                           class="btn btn-info text-light">{{ __('编辑')}}</a>
                    @endif
                </div>
            </td>


        </tr>
    @endforeach
    </tbody>
</table>