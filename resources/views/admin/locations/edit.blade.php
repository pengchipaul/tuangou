@extends('layouts.app')

@section('content')
    @include('layouts.feedback')
    <div class="container-fluid">
        @include('admin.locations.location_tabs')
        <div class="row justify-content-center">
            <div class="col-md-8">
                @component('layouts.form',array(
                'title' => '编辑地址',
                'method' => 'post',
                'action' => route('admin.locations.update',['id'=>$location->id]),
                'edit' => true,
                'data' => $location,
                'fields' => [
                    [
                        'attr' => 'name',
                        'label' => '名称',
                        'type' => 'text',
                        'required' => true
                    ],
                    [
                        'attr' => 'detail',
                        'label' => '地址详情',
                        'type' => 'text',
                        'required' => true
                    ]
                ],
                'submit_text' => '更新'
            ))
                @endcomponent
                    <a class="btn btn-secondary text-white mx-auto d-block w-25 mt-3">编辑取货地址</a>

            </div>

        </div>
    </div>
@endsection