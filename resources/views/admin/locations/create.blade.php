@extends('layouts.app')

@section('content')
    @include('layouts.feedback')
    <div class="container-fluid">
        @include('admin.locations.location_tabs')
        <div class="row justify-content-center">
            <div class="col-md-8">
                @component('layouts.form',array(
                'title' => '创建新地址',
                'method' => 'post',
                'action' => route('admin.locations.store'),
                'fields' => [
                    [
                        'attr' => 'name',
                        'label' => '名称',
                        'type' => 'text',
                        'required' => true
                    ],
                    [
                        'attr' => 'detail',
                        'label' => '地址详情',
                        'type' => 'text',
                        'required' => true
                    ]
                ],
                'submit_text' => '创建'
            ))
                @endcomponent
            </div>

        </div>
    </div>
@endsection