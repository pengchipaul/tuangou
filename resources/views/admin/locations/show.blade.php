@extends('layouts.app')

@section('content')
    @include('layouts.feedback')
    <div class="container-fluid">
        @include('admin.locations.location_tabs')
        <div class="col-md-8 offset-md-2 justify-content-center">
            <div class="card">
                <div class="card-header text-center">
                    名称:{{$location->name}}
                </div>
                <div class="card-body">
                    <p>
                        详情:{{$location->detail}}
                    </p>
                    <p>
                        取货时间:
                    </p>
                    @foreach($location->pickup_times as $pickup_time)
                        <div>{{$loop->index+1}}.{{$pickup_time->time}}</div>
                    @endforeach


                </div>
                <div class="card-footer text-right">
                    <a class="btn btn-primary text-white">编辑地址</a>
                    <a class="btn btn-secondary text-white">编辑取货时间</a>
                </div>
            </div>
        </div>
    </div>

@endsection