@component('layouts.tabs',array(
    'tabs' => [
        array(
            'link' => route('admin.locations.index'),
            'label' => __('全部地址')
        ),
        array(
            'link' => route('admin.locations.create'),
            'label' => __('创建新地址')
        )
    ]
))
@endcomponent