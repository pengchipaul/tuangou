@extends('layouts.app')

@section('content')
    @include('layouts.feedback')
    <div class="container-fluid">
        @include('admin.locations.location_tabs')
        <div class="row justify-content-center">
            <div class="col-md-12">
                @component('admin.locations.table',array(
                    'key_name' => ['ID',__('名称'),__('地址详情'),__('取货时间'),__('操作')],
                    'data_array' => $locations,
                    'key_array' => ['id','name','detail','pickup_time'],
                    'show_link' => 'admin.locations.show',
                    'edit_link' => 'admin.locations.edit'
                ))

                @endcomponent
            </div>
        </div>
    </div>

@endsection