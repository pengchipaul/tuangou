@component('layouts.tabs',array(
    'tabs' => [
        array(
            'link' => route('admin.categories.index'),
            'label' => '全部类别'
        ),
        array(
            'link' => route('admin.categories.create'),
            'label' => '创建类别'
        ),
        array(
            'link' => route('admin.categories.trashed'),
            'label' => '删除类别'
        ),
        array(
            'link' => route('admin.subcategories.index'),
            'label' => '全部子类别'
        ),
        array(
            'link' => route('admin.subcategories.create'),
            'label' => '创建子类别'
        )
    ]
))
@endcomponent