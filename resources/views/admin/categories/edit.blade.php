@extends('layouts.app')

@section('content')
    @include('layouts.feedback')
    <div class="container-fluid">
        @include('admin.categories.category_tabs')
        <div class="row justify-content-center">
            <div class="col-md-8">
                @component('layouts.form',array(
                'title' => '编辑类别',
                'method' => 'post',
                'action' => route('admin.categories.update',['id'=>$category->id]),
                'edit' => true,
                'data' => $category,
                'if_file' => true,
                'fields' => [
                    [
                        'attr'=>'image',
                        'label'=>'更新图片',
                        'type'=>'file',
                        'required'=>false
                    ],
                    [
                        'attr' => 'name',
                        'label' => '名称',
                        'type' => 'text',
                        'required' => true
                    ]
                ],
                'submit_text' => '更新'
            ))
                @endcomponent
            </div>

        </div>
    </div>
@endsection