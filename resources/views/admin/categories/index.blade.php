@extends('layouts.app')

@section('content')
    @include('layouts.feedback')
    <div class="container-fluid">
        @include('admin.categories.category_tabs')
        <div class="row justify-content-center">
            <div class="col-md-10">
                @component('layouts.table',array(
                    'key_name' => ['ID','名称','图片','操作'],
                    'data_array' => $categories,
                    'key_array' => ['id','name','image'],
                    'edit_link' => 'admin.categories.edit',
                    'show_link' => 'admin.categories.show',
                    'delete_link' => 'admin.categories.delete'
                ))

                @endcomponent
            </div>
        </div>
    </div>


@endsection