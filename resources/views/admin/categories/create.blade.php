@extends('layouts.app')

@section('content')
    @include('layouts.feedback')
    <div class="container-fluid">
        @include('admin.categories.category_tabs')
        <div class="row justify-content-center">
            <div class="col-md-8">
                @component('layouts.form',array(
                'title' => '创建新类别',
                'method' => 'post',
                'action' => route('admin.categories.store'),
                'if_file' => true,
                'fields' => [
                    [
                        'attr' => 'image',
                        'label' => '上传图片',
                        'type' =>'file',
                        'required' => true
                    ],
                    [
                        'attr' => 'name',
                        'label' => '名称',
                        'type' => 'text',
                        'required' => true
                    ]
                ],
                'submit_text' => '创建'
            ))
                @endcomponent
            </div>

        </div>
    </div>


@endsection