@extends('layouts.app')

@section('content')
    @include('layouts.feedback')

    <div class="container-fluid">
        @include('admin.categories.category_tabs')
        <div class="row justify-content-center">
            <div class="col-md-12">
                @component('layouts.table',array(
                    'key_name' => ['ID',__('名称'),__('图片'),__('操作')],
                    'data_array' => $categories,
                    'key_array' => ['id','name','image'],
                    'restore_link' => 'admin.categories.restore'
                ))

                @endcomponent
            </div>
        </div>
    </div>

@endsection