<table class="table table-bordered table-hover text-center">
    <thead>
    @foreach($key_name as $key)
        <th>
            {{$key}}
        </th>
    @endforeach
    </thead>
    <tbody>
    @foreach($data_array as $data)
        <tr>
            @foreach($key_array as $key)
                <td>
                    {{-- Customization of data --}}
                    @if(strpos($key,'img') !== false || strpos($key,'image') !== false) <a
                            href="{{asset('storage/app/'.$data[$key])}}"
                            target="_blank"><img
                                src="{{asset('storage/app/'.$data[$key])}}"
                                style="height:100px;width:100px"></a>
                    @elseif(is_bool($data[$key]))
                        @if($data[$key])
                            是
                        @else
                            否
                        @endif
                    @elseif(strpos($key, 'product_id') !== false)
                        {{$data->product->name}}
                    @else
                        {{$data[$key]}}
                    @endif
                </td>
            @endforeach
            <td>
                <div class="btn-group-vertical">
                    @if(isset($show_link))
                        <a href="{{route($show_link,['id' => $data['id']])}}"
                           class="btn btn-info text-light">{{ __('显示')}}</a>
                    @endif
                    @if(isset($edit_link))
                        <a href="{{route($edit_link,['id' => $data['id']])}}"
                           class="btn btn-info text-light">{{__('编辑')}}</a>
                    @endif
                    @if(isset($delete_link))
                        <form method="post" action="{{route($delete_link,['id'=>$data['id']])}}">
                            @csrf
                            <input name="_method" type="hidden" value="DELETE">
                            <button onclick="return confirm('确定删除吗？')" type="submit"
                                    class="btn btn-danger">{{__('删除')}}</button>
                        </form>
                    @endif
                    @if(isset($restore_link))
                        <form method="post" action="{{route($restore_link,['id'=>$data['id']])}}">
                            @csrf
                            <button onclick="return confirm('确定恢复吗？')" type="submit"
                                    class="btn btn-success">{{__('恢复')}}</button>
                        </form>
                    @endif
                </div>
            </td>


        </tr>
    @endforeach
    </tbody>
</table>