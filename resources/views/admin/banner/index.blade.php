@extends('layouts.app')

@section('content')
    @include('layouts.feedback')

    <div class="container-fluid">
        @include('admin.banner.banner_tabs')
        <div class="row justify-content-center">
            <div class="col-md-12">
                @component('admin.banner.table',array(
                    'key_name' => ['ID',__('产品名称'),__('图片'),__('操作')],
                    'data_array' => $bps,
                    'key_array' => ['id','product_id','image'],
                    'edit_link' => 'admin.banner.edit',
                    'delete_link' => 'admin.banner.delete'
                ))

                @endcomponent
            </div>
        </div>
    </div>

@endsection