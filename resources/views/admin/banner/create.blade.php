@extends('layouts.app')

@section('content')
    @include('layouts.feedback')
    <div class="container-fluid">
        @include('admin.banner.banner_tabs')
        <div class="row justify-content-center">
            <div class="col-md-8">
                @component('layouts.form',array(
                'title' => '首页产品',
                'method' => 'post',
                'action' => route('admin.banner.store'),
                'if_file' => true,
                'fields' => [
                    [
                        'attr' => 'image',
                        'label' => '首页图片',
                        'type' => 'file',
                        'required' => true
                    ],
                    [
                        'attr' => 'product_id',
                        'label' => '产品ID',
                        'type' => 'number',
                        'step' => '1',
                        'required' => true
                    ]
                ],
                'submit_text' => '上传'
            ))
                @endcomponent
            </div>

        </div>

    </div>

@endsection