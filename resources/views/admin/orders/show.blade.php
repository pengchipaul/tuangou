@extends('layouts.app')

@section('content')
    <div class="container-fluid text-center">{{$order->name}} | {{$order->phone}} | {{$order->id}} | {{$location->name}}
        ,{{$location->detail}} | {{$pickup_time->time}} </div>
    <hr>
    <div class="container-fluid w-75">
        @foreach($products as $product)
            <div class="row">
                {{$product->name}}, {{$product->get_quantity($order->id)}}个
            </div>
        @endforeach
    </div>

@endsection