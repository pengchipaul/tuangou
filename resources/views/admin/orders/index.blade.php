@extends('layouts.app')

@section('content')
    @include('layouts.feedback')
    <div class="container-fluid">
        @include('admin.orders.order_tabs')
        <div class="row justify-content-center">
            <div class="col-md-12">
                @component('admin.orders.table',array(
                    'key_name' => ['ID',__('手机号码'),__('支付方式'),__('状态'),__('总额'),__('时间'),__('取货地址'),__('操作')],
                    'data_array' => $orders,
                    'key_array' => ['id','phone','payment_method','status','total','created_at','location'],
                    'show_link' => 'admin.orders.show',
                    'release_link' => true
                ))

                @endcomponent
            </div>
        </div>
    </div>

@endsection