@extends('layouts.app')

@section('content')
    @include('layouts.feedback')
    <div class="container-fluid">
        @include('admin.orders.order_tabs')
        <div class="row justify-content-center mb-5">
            <div class="col-md-8">
                @component('layouts.form',array(
                'title' => '搜索订单',
                'method' => 'post',
                'action' => route('admin.orders.search'),
                'fields' => [
                    [
                        'attr' => 'code',
                        'label' => '验证码',
                        'type' => 'text',
                        'required' => true
                    ]
                ],
                'submit_text' => '搜索'
            ))
                @endcomponent
            </div>
        </div>
        @if(isset($result))
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            订单号:{{$order->id}} | 姓名:{{$order->name}} | 电话:{{$order->phone}} | 总价:{{$order->total}}
                        </div>
                        <div class="card-body">
                            @foreach($order->products as $product)
                                <p>商品名:{{$product->name}} ,  单价:${{$product->price}} , 数量:{{$product->get_quantity($order->id)}}</p>
                            @endforeach
                        </div>
                        <div class="card-footer">
                            <form class="single-submit-form" method="post" action="{{route('admin.orders.confirm',['id'=>$order->id])}}">
                                @csrf
                                <input type="hidden" name="_method" value="PATCH">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary mx-auto d-block single-submit-btn">确认收货</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection