<table class="table table-bordered table-hover text-center">
    <thead>
    @foreach($key_name as $key)
        <th>
            {{$key}}
        </th>
    @endforeach
    </thead>
    <tbody>
    @foreach($data_array as $data)
        <tr>
            @foreach($key_array as $key)
                <td>
                    {{-- Customization of data --}}
                    @if(strpos($key,'payment_method') !== false)
                        @if($data[$key] == 'cod')
                            货到付款
                        @elseif($data[$key] == 'wechat')
                            微信支付
                        @elseif($data[$key] == 'alipay')
                            支付宝
                        @endif
                    @elseif(strpos($key,'status') !== false)
                        @if($data[$key] == -1)
                            已作废
                        @elseif($data[$key] == 0)
                            未支付
                        @elseif($data[$key] == 1)
                            已支付
                        @elseif($data[$key] == 2)
                            已完成
                        @endif
                    @elseif(strpos($key,'location') !== false)
                        {{$data->pickup_time->location->name}} , {{$data->pickup_time->time}}
                    @elseif(is_bool($data[$key]))
                        @if($data[$key])
                            是
                        @else
                            否
                        @endif
                    @else
                        {{$data[$key]}}
                    @endif
                </td>
            @endforeach
            <td>
                <div class="btn-group-vertical">
                    @if(isset($show_link))
                        <a href="{{route($show_link,['id' => $data['id']])}}"
                           class="btn btn-info text-light">{{ __('显示')}}</a>
                    @endif
                    @if(isset($release_link) && $data['status'] == 0)
                        <form method="post" action="{{route('admin.orders.release')}}">
                            @csrf
                            <input type="hidden" name="order_id" value="{{$data['id']}}">
                            <button onclick="return confirm('确定释放吗？该订单将会标记为作废')" type="submit" class="btn btn-danger">释放库存</button>
                        </form>
                    @endif
                </div>
            </td>


        </tr>
    @endforeach
    </tbody>
</table>