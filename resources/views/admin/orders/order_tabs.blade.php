@component('layouts.tabs',array(
    'tabs' => [
        array(
            'link' => route('admin.orders.index'),
            'label' => __('全部订单')
        ),
        array(
            'link' => route('admin.orders.paid'),
            'label' => __('已支付订单')
        ),
        array(
            'link' => route('admin.orders.unpaid'),
            'label' => __('未支付订单')
        ),
        array(
            'link' => route('admin.orders.show_search'),
            'label' => __('客户提货')
        ),
    ]
))
@endcomponent