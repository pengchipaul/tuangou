@component('layouts.tabs',array(
    'tabs' => [
        array(
            'link' => route('admin.coupons.index'),
            'label' => '全部折扣卷'
        ),
        array(
            'link' => route('admin.coupons.export'),
            'label' => '导出折扣卷'
        ),
        array(
            'link' => route('admin.coupons.show_search'),
            'label' => '搜索代金券'
        )
    ]
))
@endcomponent