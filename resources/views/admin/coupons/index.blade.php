@extends('layouts.app')

@section('content')
    @include('layouts.feedback')
    <div class="container-fluid">
        @include('admin.coupons.coupon_tabs')
        <div class="row justify-content-center">
            <div class="col-md-10">
                @component('layouts.table',[
            'key_name' => ['ID',__('折扣码'),__('是否已使用'),__('数额'),__('创建日期'),__('操作')],
            'data_array' => $coupons,
            'key_array' => ['id','code','used','value','created_at']
        ])

                @endcomponent
            </div>
        </div>
    </div>

@endsection