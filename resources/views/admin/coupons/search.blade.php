@extends('layouts.app')

@section('content')
    @include('layouts.feedback')
    <div class="container-fluid">
        @include('admin.coupons.coupon_tabs')
        <div class="row justify-content-center mb-5">
            <div class="col-md-8">
                @component('layouts.form',array(
                'title' => '搜索优惠券',
                'method' => 'post',
                'action' => route('admin.coupons.search'),
                'fields' => [
                    [
                        'attr' => 'code',
                        'label' => '验证码',
                        'type' => 'text',
                        'required' => true
                    ]
                ],
                'submit_text' => '搜索'
            ))
                @endcomponent
            </div>
        </div>
        @if(isset($coupon))
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            代金券金额: {{number_format($coupon->value,2,'.','')}} | 状态: @if($coupon->used == 1)<strong
                                    class="text-danger">已使用</strong> @else <span class="text-success">未使用</span>@endif
                        </div>
                        @if($coupon->used == 0)
                            <div class="card-footer">
                                <form class="single-submit-form" method="POST"
                                      action="{{route('admin.coupons.mark',['id'=>$coupon->id])}}">
                                    @csrf
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary mx-auto d-block single-submit-btn">
                                            标记为使用
                                        </button>
                                    </div>
                                </form>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection