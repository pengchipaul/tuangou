@extends('layouts.app')

@section('content')
    @include('layouts.feedback')
    <div class="container-fluid">
        @include('admin.products.product_tabs')
        <div class="row justify-content-center">
            <div class="col-md-8">
                @component('layouts.form',array(
                'title' => '编辑产品',
                'method' => 'post',
                'action' => route('admin.products.update',['id'=>$product->id]),
                'edit' => true,
                'data' => $product,
                'if_file' => true,
                'fields' => [
                    [
                        'attr' => 'image',
                        'label' => '更新图片',
                        'type' => 'file',
                        'required' => false
                    ],
                    [
                        'attr' => 'name',
                        'label' => '名称',
                        'type' => 'text',
                        'required' => true
                    ],
                    [
                        'attr' => 'RRP',
                        'label' => 'RRP',
                        'type' => 'number',
                        'step' => '0.01',
                        'required' => true
                    ],
                    [
                        'attr' => 'price',
                        'label' => '价格',
                        'type' => 'number',
                        'step' => '0.01',
                        'required' => true
                    ],
                    [
                        'attr' => 'deadline',
                        'label' => '截至日期',
                        'type' => 'date',
                        'required' => true
                    ],
                    [
                        'attr' => 'stock',
                        'label' => '库存',
                        'type' => 'number',
                        'step' => '1',
                        'required' => true
                    ],
                    [
                        'attr' => 'sales',
                        'label' => '显示销量',
                        'type' => 'number',
                        'step' => '1',
                        'required' => true
                    ],
                    [
                        'attr' => 'barcode',
                        'label' => '银豹条码',
                        'type' => 'number',
                        'step' => '1',
                        'required' => false
                    ],
                    [
                        'attr' => 'category_id',
                        'label' => '类别',
                        'type' => 'select',
                        'options' => $category_options,
                        'required' => true
                    ],
                    [
                        'attr' => 'subcategory_id',
                        'label' => '子类别',
                        'type' => 'select',
                        'options' => $subcategory_options,
                        'required' => false,
                    ]
                ],
                'submit_text' => '更新'
            ))
                @endcomponent
            </div>

        </div>
    </div>


@endsection