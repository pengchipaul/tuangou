@extends('layouts.app')

@section('content')
    @include('layouts.feedback')

    <div class="container-fluid">
        @include('admin.products.product_tabs')
        <div class="row justify-content-center">
            <div class="col-md-12">
                @component('layouts.table',array(
                    'key_name' => ['ID',__('名称'),__('图片'),'RRP',__('价格'),__('截至日期'),__('显示销量'),__('实际销量'),__('库存'),__('类别'),__('子类别'),__('操作')],
                    'data_array' => $products,
                    'key_array' => ['id','name','image','RRP','price','deadline','sales','real_sales','stock','category_id','subcategory_id'],
                    'edit_link' => 'admin.products.edit',
                    'restore_link' => 'admin.products.restore'
                ))

                @endcomponent
            </div>
        </div>
    </div>

@endsection