@component('layouts.tabs',array(
    'tabs' => [
        array(
            'link' => route('admin.products.index'),
            'label' => __('全部产品')
        ),
        array(
            'link' => route('admin.products.trashed'),
            'label' => __('删除产品')
        ),
        array(
            'link' => route('admin.products.create'),
            'label' => __('创建产品')
        )
    ]
))
@endcomponent