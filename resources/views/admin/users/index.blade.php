@extends('layouts.app')

@section('content')
    @include('layouts.feedback')
    <div class="container-fluid">
        @include('admin.users.user_tabs')
        @component('layouts.table',array(
            'key_name' => ['ID',__('用户名'),__('电话/登录名'),__('管理员'),__('员工'),__('操作')],
            'data_array' => $users,
            'key_array' => ['id','name','phone','admin','staff']
        ))

        @endcomponent

    </div>
@endsection