@extends('layouts.app')

@section('content')
    @include('layouts.feedback')
    <div class="container-fluid">
        @include('admin.users.user_tabs')
        <div class="row justify-content-center">
            <div class="col-md-8">
                @component('layouts.form',array(
                    'title' => '创建新用户',
                    'method' => 'post',
                    'action' => route('admin.users.store'),
                    'if_file' => false,
                    'fields' => [
                        array(
                            'attr' => 'name',
                            'label' => '用户名',
                            'type' => 'text',
                            'required' => true
                        ),
                        array(
                            'attr' => 'phone',
                            'label' => '登录名',
                            'type' => 'text',
                            'required' => true,
                        ),
                        array(
                            'attr' => 'password',
                            'label' => '密码',
                            'type' => 'password',
                            'required' => true
                        ),
                        array(
                            'attr' => 'admin',
                            'label' => '是否是管理员',
                            'type' => 'select',
                            'options' => [
                                array(
                                    'value' => 0,
                                    'text' => '否'
                                ),
                                array(
                                    'value' => 1,
                                    'text' => '是'
                                )
                            ],
                            'required' => true
                        ),
                        array(
                            'attr' => 'staff',
                            'label' => '员工职务',
                            'type' => 'select',
                            'options' => [
                                array(
                                    'value' => 0,
                                    'text' => '没有职务'
                                ),
                                array(
                                    'value' => 1,
                                    'text' => '配送员'
                                )
                            ],
                            'required' => true
                        )
                    ],
                    'submit_text' => '创建'
                ))
                @endcomponent
            </div>
        </div>
    </div>

@endsection