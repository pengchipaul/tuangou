@component('layouts.tabs',array(
    'tabs' => [
        array(
            'link' => route('admin.users.index'),
            'label' => __('全部用户')
        ),
        array(
            'link' => route('admin.users.staff_index'),
            'label' => __('员工账户')
        ),
        array(
            'link' => route('admin.users.create'),
            'label' => __('创建用户')
        )
    ]
))
@endcomponent