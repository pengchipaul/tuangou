@extends('layouts.app')

@section('content')
    @include('layouts.feedback')
    <div class="container-fluid">
        @include('admin.categories.category_tabs')
        <div class="row justify-content-center">
            <div class="col-md-10">
                @component('layouts.table',array(
                    'key_name' => ['ID','名称','类别','操作'],
                    'data_array' => $subcategories,
                    'key_array' => ['id','name','category_id'],
                    'edit_link' => 'admin.subcategories.edit',
                    'show_link' => 'admin.subcategories.show',
                    'delete_link' => 'admin.subcategories.delete'
                ))

                @endcomponent
            </div>
        </div>
    </div>


@endsection