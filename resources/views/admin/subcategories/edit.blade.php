@extends('layouts.app')

@section('content')
    @include('layouts.feedback')
    <div class="container-fluid">
        @include('admin.categories.category_tabs')
        <div class="row justify-content-center">
            <div class="col-md-8">
                @component('layouts.form',array(
                'title' => '编辑子类别',
                'method' => 'post',
                'edit' => true,
                'data' => $subcategory,
                'action' => route('admin.subcategories.update',['id'=>$subcategory->id]),
                'fields' => [
                    [
                        'attr' => 'name',
                        'label' => '名称',
                        'type' => 'text',
                        'required' => true
                    ],
                    [
                        'attr' => 'category_id',
                        'label' => '类别',
                        'type' => 'select',
                        'options' => $category_options,
                        'required'=>true
                    ]
                ],
                'submit_text' => '创建'
            ))
                @endcomponent
            </div>

        </div>
    </div>


@endsection