e<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Location;
use App\PickupTime;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        try{
            User::create([
                'name'=>'admin',
                'email'=>'admin',
                'phone'=>'admin',
                'password'=>Hash::make('test123'),
                'admin'=>true
            ]);
        } catch(Exception $e){

        }

        $location = Location::create([
            'name' => '唐旗国际',
            'detail' => '611 Station St, Box Hill VIC 3128 \n 门店自提From 11:00-18:00'
        ]);
        PickupTime::create([
            'time' => '下单后一个工作日自提',
            'location_id' => $location->id
        ]);
        $location = Location::create([
            'name' => 'First Shopping Clayton',
            'detail' => '22 Winterton Rd, Clayton VIC 3168 \n 门店自提From 10:00-18:00'
        ]);
        PickupTime::create([
            'time' => '下单后一个工作日自提',
            'location_id' => $location->id
        ]);
        $location = Location::create([
            'name' => 'First Shopping Carnegie',
            'detail' => '135A Koornang Rd, Carnegie VIC 3163 \n 门店自提From 11:00-18:00'
        ]);
        PickupTime::create([
            'time' => '下单后一个工作日自提',
            'location_id' => $location->id
        ]);
        $location =Location::create([
            'name' => 'Monash Caulfield Campus',
            'detail' => 'H楼门口'
        ]);
        PickupTime::create([
            'time' => '7月19日下午13.00',
            'location_id' => $location->id
        ]);
        PickupTime::create([
            'time' => '7月21日下午13.00',
            'location_id' => $location->id
        ]);
        $location = Location::create([
            'name' => 'Monash Clayton Campus',
            'detail' => 'Robert Blackwood Hall门口'
        ]);
        PickupTime::create([
            'time' => '7月19日下午13.00',
            'location_id' => $location->id
        ]);
        PickupTime::create([
            'time' => '7月21日下午13.00',
            'location_id' => $location->id
        ]);
        $location = Location::create([
            'name' => 'Melbourne Uni and RMIT',
            'detail' => '脸楼门口'
        ]);
        PickupTime::create([
            'time' => '7月19号早上11:00',
            'location_id' => $location->id
        ]);
    }
}
