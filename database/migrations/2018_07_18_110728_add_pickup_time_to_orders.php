<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPickupTimeToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders',function(Blueprint $table){
            $table->unsignedInteger('pickup_time_id');

            $table->foreign('pickup_time_id')->references('id')->on('pickup_times');

            $table->dropForeign('orders_location_id_foreign');
            $table->dropColumn('location_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders',function(Blueprint $table){
            $table->unsignedInteger('location_id');
            $table->foreign('location_id')->references('id')->on('locations');

            $table->dropForeign('orders_pickup_time_id_foreign');
            $table->dropColumn('pickup_time_id');
        });
    }
}
