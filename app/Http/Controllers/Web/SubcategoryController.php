<?php

namespace App\Http\Controllers\Web;

use App\Subcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubcategoryController extends Controller
{
    public function get_products($id){
        $subcategory = Subcategory::find($id);
        $products = $subcategory->products;
        return response()->json($products,200);
    }
}
