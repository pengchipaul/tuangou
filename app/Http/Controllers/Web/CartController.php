<?php

namespace App\Http\Controllers\Web;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class CartController extends Controller
{
    public function __construct()
    {
        if(!session()->has('cart')){
            session()->put('cart',array());
        }
    }

    public function show(){
        $full_screen = true;
        $products = Product::all();
        if($this->is_store()){
            $view = 'web.retail.cart.show';
        } else {
            $view = 'web.mobile.cart.show';
        }
        return view($view,compact('products','full_screen'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add_to_cart(Request $request){
        /* validate request */
        if(!isset($request->product_id) || !isset($request->quantity)){
            return response()->json(['message'=>'error','data'=>'format incorrect'],403);
        }
        $product_id = $request->product_id;
        $quantity = $request->quantity;
        if($quantity <= 0){
            return response()->json(['message'=>'error','data'=>'invalid quantity'],403);
        }
        try{
            $product = Product::find($product_id);
            $stock = $product->stock;
        } catch(\Exception $e){
            Log::debug($e);
            return response()->json(['message'=>'error','data'=>'product not found'],403);
        }

        /* update cart */
        $cart = session()->pull('cart');
        if(array_key_exists($product_id,$cart)){
            $cart[$product_id] += $quantity;
            $cart[$product_id] > $stock ? $cart[$product_id] = $stock : null;
            session()->put('cart',$cart);
        } else {
            $cart[$product_id] = $quantity;
            session()->put('cart',$cart);
        }
        return response()->json(['message'=>'success','data'=>'添加成功'],200);
    }

    public function minus_from_cart(Request $request){
        /* validate request */
        if(!isset($request->product_id) || !isset($request->quantity)){
            return response()->json(['message'=>'error','data'=>'format incorrect'],403);
        }
        $product_id = $request->product_id;
        $quantity = $request->quantity;
        if($quantity <= 0){
            return response()->json(['message'=>'error','data'=>'invalid quantity'],403);
        }
        try{
            Product::find($product_id);
        } catch(\Exception $e){
            Log::debug($e);
            return response()->json(['message'=>'error','data'=>'product not found'],403);
        }

        /* update cart */
        $cart = session()->pull('cart');
        if(array_key_exists($product_id,$cart)){
            $cart[$product_id] -= $quantity;
            if($cart[$product_id] <= 0){
                unset($cart[$product_id]);
            }
            session()->put('cart',$cart);
        } else {
            return response()->json(['message'=>'success','data'=>'移除成功'],200);
        }
        return response()->json(['message'=>'success','data'=>'移除成功'],200);
    }

    public function update_cart_item(Request $request){
        /* validate request data */
        $product_id = $request->get('product_id');
        $quantity = $request->get('quantity');
        if($quantity <= 0){
            return response()->json(['message'=>'fail'],403);
        }
        $cart = session()->get('cart');
        if(array_key_exists($product_id,$cart)){
            $cart[$product_id] = $quantity;
            session()->put('cart',$cart);
            return response()->json(['message'=>'success'],200);
        } else {
            return response()->json(['message'=>'fail'],403);
        }
    }

    public function submit_cart(Request $request){
        /* validate request */
        if(!$request->has('products')){
            return redirect(route('users.show_cart'));
        }
        $cart = session()->get('cart');
        $update_cart = $request->get('products');
        foreach(array_keys($cart) as $product_id){
            try{
                $quantity = intval($update_cart[$product_id]);
                if($quantity == 0){
                    return redirect(route('users.show_cart'));
                }
                $cart[$product_id] = $quantity;
            } catch(\Exception $e){
                Log::debug($e);
                return redirect(route('users.show_cart'));
            }
        }

        /* update cart */
        session()->put('cart',$cart);

        return redirect(route('users.orders.show_form'));
    }

    /**
     * @param $product_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete_cart_item($product_id){
        try{
            Product::findOrFail($product_id);
        } catch(\Exception $e){
            Log::debug($e);
            return response()->json(['message'=>'error','data'=>'product not found'],403);
        }

        $cart = session()->pull('cart');
        if(array_key_exists($product_id,$cart)){
            unset($cart[$product_id]);
            session()->put('cart',$cart);
            return response()->json(['message'=>'success'],200);
        } else {
            Log::debug("********** MALICIOUS USER INPUT **********");
            Log::debug("Someone tried to delete a cart item that didn't exist");
            session()->put('cart',$cart);
            return response()->json(['message'=>'error','data'=>'item not in cart'],403);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function clear_cart(){
        session()->pull('cart');
        return response()->json(['message'=>'success','data'=>'cart cleared'],200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_cart(){
        $cart = session()->get('cart');
        if(count($cart) != 0){
            $products = Product::whereIn('id',array_keys($cart))->get();
            foreach($products as $product){
                $product['quantity'] = $cart[$product->id];
            }
            return response()->json(['message'=>'success','data'=>$products],200);
        } else {
            return response()->json(['message'=>'success','data'=>null],200);
        }
    }
}
