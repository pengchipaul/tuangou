<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Jenssegers\Agent\Agent;

use App\BannerProducts;

class HomeController extends Controller
{
    private $device;

    public function __construct()
    {
        /**
         * determine the user agent for displaying different view
         */
        $agent = new Agent();
        if($agent->isMobile()){
            $this->device = "mobile";
        } elseif($agent->isTablet()){
            $this->device = "tablet";
        } elseif($agent->isDesktop()){
            $this->device = "desktop";
        }
    }

    public function change_language($locale){
        Session::put('applocale',$locale);
        return redirect()->back();
    }

    public function index(){
        $full_screen = true;
        return view('web.'.$this->device.'.home',compact('full_screen'));
    }

    public function enter($lang){
        Session::put('applocale',$lang);
        return redirect(route('users.products.index'));
    }

    public function banner_products(){
        $bps = BannerProducts::all();
        return response()->json($bps,200);
    }

}
