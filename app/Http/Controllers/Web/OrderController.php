<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use CodeItNow\BarcodeBundle\Utils\QrCode;

use App\Notifications\NewOrder;
use App\OrderProduct;
use App\PickupTime;
use App\Product;
use App\Order;
use App\Coupon;
use App\Jobs\OrderNotify;
use App\Location;

class OrderController extends Controller
{
    /**
     * show order info form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show_form()
    {
        $full_screen = true;
        $locations = Location::all();
        foreach ($locations as $location) {
            $location['pickup_times'] = $location->pickup_times;
        }
        if ($this->is_store()) {
            $view = 'web.retail.orders.form';
        } else {
            $view = 'web.mobile.orders.form';
        }
        if (session()->has('order_info')) {
            $order_info = session()->get('order_info');
            return view($view, compact('full_screen', 'locations', 'order_info'));
        }
        return view($view, compact('full_screen', 'locations'));
    }

    public function submit(Request $request)
    {

        /* validate request data */
        $data_array = $request->all();
        $validator = $this->validator($data_array);
        if ($validator->fails()) {
            return redirect(route('users.orders.show_form'))->withErrors($validator)->withInput();
        }

        /* validate coupon */
        /*$use_coupon = false;
        if ($data_array['coupon'] != null) {
            $code = $data_array['coupon'];
            if (Coupon::where('code', $code)->count() == 0) {
                $validator->errors()->add('coupon', '优惠卷码无效');
                return redirect(route('users.orders.show_form'))->withErrors($validator)->withInput();
            }
            $coupon = Coupon::where('code', $code)->first();
            if ($coupon->used == true) {
                $validator->errors()->add('coupon', '优惠卷码无效');
                return redirect(route('users.orders.show_form'))->withErrors($validator)->withInput();
            }
            $use_coupon = true;
        }*/

        /* store order form info */
        $order_info = array();
        $order_info['phone'] = $data_array['phone'];
        $order_info['name'] = $data_array['name'];
        $order_info['payment_method'] = $data_array['payment_method'];
        $order_info['pickup_time_id'] = $data_array['pickup_time_id'];
        /* calculate order */
        $cart = session()->get('cart');
        $products = Product::whereIn('id', array_keys($cart))->get();
        $total = 0;
        foreach ($products as $product) {
            $product['quantity'] = $cart[$product->id];
            $product['total'] = number_format($product->price * $product['quantity'], 2, '.', '');
            $total += $product->price * $product['quantity'];
        }
        $total = number_format($total, 2, '.', '');
        $order_info['total'] = $total;

        /* apply coupon */
        /*if ($use_coupon == true) {
            $order_info['coupon'] = $data_array['coupon'];
            $order_info['discount'] = $coupon->value;
            $order_info['total'] -= $coupon->value;
        }*/
        session()->put('order_info', $order_info);

        return redirect(route('users.orders.confirm'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function confirm()
    {
        $full_screen = true;
        /* cart info */
        $cart = session()->get('cart');
        $products = Product::whereIn('id', array_keys($cart))->get();
        foreach ($products as $product) {
            $product['quantity'] = $cart[$product->id];
            $product['total'] = number_format($product->price * $product['quantity'], 2, '.', '');
        }

        /* order info */
        $order_info = session()->get('order_info');
        $pickup_time = PickupTime::find($order_info['pickup_time_id']);
        $location = $pickup_time->location;
        if ($this->is_store()) {
            $view = 'web.retail.orders.confirm';
        } else {
            $view = 'web.mobile.orders.confirm';
        }
        return view($view, compact('products', 'full_screen', 'order_info', 'location', 'pickup_time'));
    }

    public function pay()
    {

        $order_info = session()->get('order_info');
        $cart = session()->get('cart');

        /* check if coupon is valid */
        if (key_exists('coupon', $order_info)) {
            $coupon_code = $order_info['coupon'];
            $coupon = Coupon::where('code', $coupon_code)->first();
            if ($coupon->used == true) {
                return redirect(route('users.orders.show_form'));
            }
        }

        /* check if stock is available */
        foreach (array_keys($cart) as $product_id) {
            $product = Product::find($product_id);
            if ($cart[$product_id] > $product->stock) {
                return redirect(route('users.show_cart'));
            }
        }

        $order = $this->create_order($order_info, $cart);

        session()->pull('order_info');
        session()->pull('cart');

        if ($order_info['payment_method'] == "wechat") {
            return $this->wechat_pay($order);
        }
        if ($order_info['payment_method'] == "alipay") {
            return $this->alipay($order);
        }
        if ($order_info['payment_method'] == "cod") {
            return $this->cod($order);
        }
        return redirect(route('users.orders.confirm'));
    }

    public function payment_result()
    {
        $full_screen = true;
        if ($this->is_store()) {
            $view = 'web.retail.orders.cod_result';
        } else {
            $view = 'web.mobile.orders.cod_result';
        }
        return view($view, compact('full_screen'));
    }

    public function create_order($order_info, $cart)
    {
        $lang = 'cn';
        /* create order */
        try {
            $order = Order::create([
                'name' => $order_info['name'],
                'phone' => $order_info['phone'],
                'payment_method' => $order_info['payment_method'],
                'pickup_time_id' => $order_info['pickup_time_id'],
                'total' => $order_info['total'],
                'lang' => $lang
            ]);
        } catch (\Exception $e) {
            return redirect(route('users.orders.confirm'));
        }

        /* create products in order and deduct stocks */
        foreach (array_keys($cart) as $product_id) {
            try {
                OrderProduct::create([
                    'order_id' => $order->id,
                    'product_id' => $product_id,
                    'quantity' => $cart[$product_id]
                ]);
                $product = Product::find($product_id);
                $product->stock -= $cart[$product_id];
                $product->save();
            } catch (\Exception $e) {
                Log::debug($e);
            }
        }

        /* mark coupon as used */
        if (isset($order_info['coupon'])) {
            $coupon = Coupon::where('code', $order_info['coupon'])->first();
            $coupon->used = true;
            $coupon->phone = $order_info['phone'];
            $coupon->save();
        }

        return $order;
    }

    public function wechat_pay($order)
    {
        $pic_url = PaymentController::wechat_pay($order);
        if ($pic_url == null) {
            return redirect(route('index'));
        }
        $qr_code = new QrCode();
        try {
            $qr_code
                ->setText($pic_url)
                ->setSize(500)
                ->setPadding(10)
                ->setErrorCorrection('high')
                ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
                ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
                ->setImageType(QrCode::IMAGE_TYPE_PNG);
        } catch (\Exception $e) {
            Log::debug($e);
            return redirect(route('index'));
        }
        Storage::disk('local')->put('orders/QRcode/' . $order->id . '.png', base64_decode($qr_code->generate()));
        $pic_url = asset('storage/app/orders/QRcode/' . $order->id . '.png');

        if ($this->is_store()) {
            $view = 'web.retail.payment.QR';
        } else {
            $view = 'web.mobile.payment.AliQR';
        }
        $full_screen = true;
        return view($view, compact('pic_url', 'full_screen'));
    }

    public function alipay($order)
    {
        $pic_url = PaymentController::alipay($order);
        if ($pic_url == null) {
            return redirect(route('index'));
        }
        if ($this->is_store()) {
            $view = 'web.retail.payment.QR';
        } else {
            $view = 'web.mobile.payment.AliQR';
        }
        $full_screen = true;
        return view($view, compact('pic_url', 'full_screen'));
    }

    public function cod($order)
    {
        OrderNotify::dispatch($order);
        return redirect(route('users.orders.result'));
    }

    /**
     * @param $data_array
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator($data_array)
    {
        $pickup_times = PickupTime::all();
        $values = array();
        foreach ($pickup_times as $pickup_time) {
            $values[] = $pickup_time->id;
        }
        return Validator::make($data_array, [
            'phone' => 'required|digits:10',
            'name' => 'required|string|max:40',
            'coupon' => 'nullable|string|size:6',
            'payment_method' => ['required', Rule::in(['wechat', 'alipay', 'cod'])],
            'pickup_time_id' => ['required', Rule::in($values)]
        ]);
    }
}
