<?php

namespace App\Http\Controllers\Web;

use App\Notifications\ReportBug;
use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use App\Jobs\OrderNotify;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class PaymentController extends Controller
{
    use SerializesModels;

    public static function wechat_pay(Order $order)
    {
        $trade_id = sprintf("%0".config('order.length')."d", $order->id);
        $content = array(
            'mch_id' => config('paylinx.mch_id'),
            'store_id' => config('paylinx.store_id'),
            'nonce_str' => 'ozkvr95oar40qa43n13rngoso9yge97x',
            'notify_url' => 'http://tuangou.tangqi.com.au/payment_verify/'.config('paylinx.verify_key').'/verify',
            'return_url' => 'http://tuangou.tangqi.com.au',
            'out_trade_no' => $trade_id,
            'body' => 'firstshopping_online',
            'spbill_create_ip' => '43.255.154.108',
            'total_fee' => $order->total * 100,
            'fee_type' => 'AUD',
            'trade_type' => 'NATIVE'
        );
        ksort($content);
        $queryStr = '';
        foreach ($content as $key => $val) {
            if ((string)$val === '') continue;
            $queryStr .= $key . '=' . $val . '&';
        }
        $queryStr .= 'key=' . config('paylinx.app_key');
        $sign = strtoupper(md5($queryStr));
        $content['sign'] = $sign;
        $post = PaymentController::array_to_xml($content);
        $client = new Client();
        $res = $client->post('http://paylinx.cn/wxpay/gateway/unifiedorder/', [
            'body' => $post
        ]);
        $xml = new \SimpleXMLElement($res->getBody());
        if ($xml->result_code == "SUCCESS") {
            $order->payment_link = $xml->code_url;
            $order->save();
            return $xml->code_url;
        }
        return null;
    }

    public static function alipay($order)
    {
        $trade_id = sprintf("%011d", $order->id);
        $total_amount = $order->total;
        $content = array(
            'partner' => config('paylinx.alipay_mch_id'),
            'store_id' => config('paylinx.store_id'),
            'notify_url' => 'http://tuangou.tangqi.com.au/payment_verify/'.config('paylinx.verify_key').'/verify',
            'out_trade_no' => $trade_id,
            'subject' => 'firstshopping_tuangou',
            'total_fee' => $total_amount * 100,
            'trans_currency' => 'AUD',
        );
        $queryStr = '';
        ksort($content);
        foreach ($content as $key => $val) {
            $queryStr .= $key . '=' . $val . '&';
        }
        $queryStr .= 'key=' . config('paylinx.app_key');
        $sign = strtoupper(md5($queryStr));
        $content['sign'] = $sign;
        $post = PaymentController::array_to_XML($content);
        $client = new Client();
        $res = $client->post('http://paylinx.cn/alipay/gateway/create/', [
            'body' => $post
        ]);
        $xml = new \SimpleXMLElement($res->getBody());
        if ($xml->result_code == "SUCCESS") {
            $order->payment_link = $xml->pic_url;
            $order->save();
            return $xml->pic_url;
        }
        return null;
    }

    public function verify(Request $request)
    {
        $data = file_get_contents('php://input');
        Log::debug("data");
        Log::debug($data);
        $xml = new \SimpleXMLElement($data);
        if($xml->trade_status == "TRADE_SUCCESS" || $xml->result_code == "SUCCESS"){
            $id = $xml->out_trade_no;
            $order = Order::find($id);
            if($order->status == 0){
                $order->status = 1;
                $order->save();
                OrderNotify::dispatch($order);
                $products = $order->products;
                foreach ($products as $product) {
                    $quantity = OrderProduct::where('order_id',$order->id)->where('product_id',$product->id)->first()->quantity;
                    $product->sales += $quantity;
                    $product->real_sales += $quantity;
                    $product->save();
                }
            }
        }
    }



    public static function array_to_xml($array)
    {
        $xml = "<?xml version='1.0'?>";
        $xml .= "<xml>";
        foreach ($array as $key => $value) {
            $xml .= "<" . $key . ">" . $value . "</" . $key . ">";
        }
        $xml .= "</xml>";
        return $xml;
    }
}
