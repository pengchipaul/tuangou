<?php

namespace App\Http\Controllers\Web;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Jenssegers\Agent\Agent;

class ProductController extends Controller
{
    private $device;

    public function __construct()
    {
        /**
         * determine the user agent for displaying different view
         */
        $agent = new Agent();
        if($agent->isMobile()){
            $this->device = "mobile";
        } elseif($agent->isTablet()){
            $this->device = "tablet";
        } elseif($agent->isDesktop()){
            $this->device = "desktop";
        }
    }

    public function index(){
        $categories = Category::all();
        $full_screen = true;
        if($this->is_store()){
            $view = 'web.retail.products.index';
        } else {
            if($this->device != "mobile"){
                return redirect(route('home'));
            }
            $view = 'web.'.$this->device.'.products.index';
        }
        return view($view,compact('categories','full_screen'));
    }

    public function get_products(){
        if($this->is_store()){
            $products = Product::all();
        } else {
            $products = Product::orderBy('id')->paginate(10);
        }
        return response()->json($products,200);
    }

    public function get_product($id){
        try{
            $product = Product::findOrFail($id);
            return response()->json($product,200);
        }catch(\Exception $e){
            Log::debug($e);
            return response()->json('not found',403);
        }
    }
}
