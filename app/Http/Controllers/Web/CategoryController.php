<?php

namespace App\Http\Controllers\Web;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function get_products($id){
        $category = Category::find($id);
        if($this->is_store()){
            $products = $category->products;
        } else {
            $products = $category->products()->orderBy('id')->paginate(10);
        }
        return response()->json($products, 200);
    }
}
