<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ViewController extends Controller
{
    public function page(){
        $pic_url = asset('public/assets/gift.png');
        return view('web.retail.payment.QR',compact('pic_url'));
    }
}
