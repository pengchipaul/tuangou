<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\OrderProduct;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Notification;
use App\Notifications\SendCoupon;
use Illuminate\Support\Facades\Log;
use App\Jobs\BugNotify;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::all();
        return view('admin.orders.index', compact('orders'));
    }

    public function paid()
    {
        $orders = Order::where('status', '>', '0')->get();
        return view('admin.orders.index', compact('orders'));
    }

    public function unpaid()
    {
        $orders = Order::where('status', '0')->get();
        return view('admin.orders.index', compact('orders'));
    }

    public function show($id)
    {
        $order = Order::find($id);
        $products = $order->products;
        $pickup_time = $order->pickup_time;
        $location = $pickup_time->location;
        return view('admin.orders.show', compact('order', 'products', 'pickup_time', 'location'));
    }

    public function show_search()
    {
        return view('admin.orders.search_result');
    }

    public function search(Request $request)
    {
        $code = $request->code;
        $order = Order::where('code', $code)->whereIn('status',[0,1])->first();
        if ($order == null) {
            return redirect(route('admin.orders.show_search'))->with('fail', '未找到相应订单');
        }
        $result = true;
        return view('admin.orders.search_result', compact('order', 'result'));
    }

    public function confirm($id)
    {
        $order = Order::find($id);
        if ($order->status == 0 || $order->status == 1) {
            $order->status = 2;
            $order->save();
        } else {
            return redirect(route('admin.orders.show_search'))->with('fail', '订单状态已完成');
        }
        $products = $order->products;
        foreach ($products as $product) {
            $quantity = OrderProduct::where('order_id', $order->id)->where('product_id', $product->id)->first()->quantity;
            $product->sales += $quantity;
            $product->real_sales += $quantity;
            $product->save();
        }
        if($order->status == 0){
            $aus_num = ltrim($order->phone,"0");
            $aus_num = "61".$aus_num;
            try{
                Notification::route('nexmo',$aus_num)->notify(new SendCoupon($order->id));
            } catch (\Exception $e){
                Log::debug($e);
                BugNotify::dispatch(strval($e),"sending sms to customer mobile phone ".$aus_num);
            }
        }
        return redirect(route('admin.orders.show_search'))->with('success', '提货成功');
    }

    public function release_stock(Request $request){
        $order = Order::find($request->order_id);
        if($order->status != 0){
            return redirect()->back()->with('fail','不能释放改订单库存');
        }
        foreach($order->order_products as $order_product){
            $product = Product::find($order_product->product_id);
            $product->stock += $order_product->quantity;
            $product->save();
        }
        $order->status = -1;
        $order->save();
        return redirect(route('admin.orders.index'))->with('success','已释放库存');
    }
}
