<?php

namespace App\Http\Controllers\Admin;

use App\Location;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class LocationController extends Controller
{
    public function index(){
        $locations = Location::all();
        return view('admin.locations.index',compact('locations'));
    }

    public function show($id){
        $location = Location::find($id);
        return view('admin.locations.show',compact('location'));
    }

    public function create(){
        return view('admin.locations.create');
    }

    public function store(Request $request){
        try{
            Location::create($request->all());
            return redirect(route('admin.locations.index'))->with('success','创建新地址成功');
        } catch(\Exception $e){
            Log::debug($e);
            return redirect(route('admin.locations.create'))->withInput()->with('fail','创建新地址失败');
        }
    }

    public function edit($id){
        $location = Location::find($id);
        return view('admin.locations.edit',compact('location'));
    }

    public function update($id, Request $request){
        $location = Location::find($id);
        try{
            $location->update($request->all());
            return redirect(route('admin.locations.edit',['id'=>$location->id]))->with('success','更新地址成功');
        } catch(\Exception $e){
            Log::debug($e);
            return redirect(route('admin.locations.edit',['id'=>$location->id]))->with('fail','更新地址失败');
        }
    }

    public function delete($id){

    }
}
