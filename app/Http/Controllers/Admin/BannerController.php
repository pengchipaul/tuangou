<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

use App\BannerProducts;
use App\Product;

class BannerController extends Controller
{
    public function index(){
        $bps = BannerProducts::all();
        return view('admin.banner.index',compact('bps'));
    }

    public function create(){
        return view('admin.banner.create');
    }

    public function store(Request $request){
        try{
            Product::findOrFail($request->product_id);
        } catch(\Exception $e){
            Log::debug($e);
            return redirect(route('admin.banner.index'))->with('fail','未找到商品');
        }
        if($request->hasFile('image')){
            if($request->file('image')->isValid()){
                try{
                    $img_path = $request->file('image')->store('banner');
                } catch (\Exception $e){
                    Log::debug($e);
                    return redirect(route('admin.banner.create'))->with('fail','图片上传错误')->withInput();
                }
            } else {
                return redirect(route('admin.banner.create'))->with('fail','图片上传错误')->withInput();
            }
        } else {
            return redirect(route('admin.banner.create'))->with('fail','图片上传错误')->withInput();
        }
        try{
            BannerProducts::create(['product_id'=>$request->product_id,'image'=>$img_path]);
        } catch(\Exception $e){
            return redirect(route('admin.banner.create'))->with('fail','Banner上传错误')->withInput();
        }
        return redirect(route('admin.banner.index'))->with('success','添加成功');
    }

    public function show($id){
        try{
            $bp = BannerProducts::findOrFail($id);
        } catch(\Exception $e){
            Log::debug($e);
            return redirect(route('admin.banner.index'));
        }

        return view('admin.banner.show',compact('bp'));
    }

    public function edit($id){
        try{
            $bp = BannerProducts::findOrFail($id);
        } catch(\Exception $e){
            Log::debug($e);
            return redirect(route('admin.banner.index'));
        }

        return view('admin.banner.edit',compact('bp'));
    }

    public function update($id, Request $request){

    }

    public function delete($id){
        try{
            $bp = BannerProducts::findOrFail($id);
            Storage::delete($bp->image);
            $bp->delete();
            return redirect(route('admin.banner.index'))->with('success','删除成功');
        } catch(\Exception $e){
            Log::debug($e);
            return redirect(route('admin.banner.index'))->with('fail','删除失败');
        }
    }
}
