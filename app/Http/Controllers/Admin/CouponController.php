<?php

namespace App\Http\Controllers\Admin;

use App\Coupon;
use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class CouponController extends Controller
{
    public function index()
    {
        $coupons = Coupon::all();
        return view('admin.coupons.index', compact('coupons'));
    }

    public function export()
    {
        $content = array();

        /* generate headings */
        $content[] = ("ID,折扣码,是否使用,数额,创建日期");

        $coupons = Coupon::all();
        foreach ($coupons as $coupon) {
            if ($coupon->used) {
                $coupon['if_used'] = "是";
            } else {
                $coupon['if_used'] = "否";
            }
            $content[] = ($coupon->id . "," . $coupon->code . "," . $coupon['if_used'] . "," . $coupon->value . "," . $coupon->created_at);
        }
        $content = implode("\n", $content);
        Storage::disk('public')->put('admin/coupon.csv', $content);
        return Storage::download('public/admin/coupon.csv');
    }

    public function show_search()
    {
        return view('admin.coupons.search');
    }

    public function search(Request $request)
    {
        $code = $request->code;
        $coupon = Coupon::where('code', $code)->get();
        if (count($coupon) == 0) {
            return redirect(route('admin.coupons.show_search'))->with('fail', '未找到优惠券');
        } else {
            $coupon = $coupon->first();
            return view('admin.coupons.search', compact('coupon'));
        }
    }

    public function mark($id)
    {
        $coupon = Coupon::find($id);
        if ($coupon == null) {
            return redirect(route('admin.coupons.show_search'))->with('fail', '标记失败');
        }
        if ($coupon->used == true) {
            return redirect(route('admin.coupons.show_search'))->with('fail', '标记失败');
        }
        $coupon->used = true;
        $coupon->save();
        return redirect(route('admin.coupons.show_search'))->with('success', '标记成功');
    }
}
