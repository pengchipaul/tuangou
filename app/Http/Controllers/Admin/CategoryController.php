<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('admin.categories.index', compact('categories'));
    }

    public function show($id)
    {

    }

    public function create()
    {
        return view('admin.categories.create');
    }

    public function store(Request $request)
    {
        $data_array = $request->all();
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $img_path = $request->file('image')->store('categories/images');
            } else {
                return redirect(route('admin.categories.index'))->with('fail', '图片无效');
            }
        } else {
            return redirect(route('admin.categories.index'))->with('fail', '获取图片失败');
        }
        if (isset($img_path)) {
            $data_array['image'] = $img_path;
        }
        try {
            Category::create($data_array);
            return redirect(route('admin.categories.index'))->with('success', '类别创建成功');
        } catch (\Exception $e) {
            Log::debug($e);
            return redirect(route('admin.categories.index'))->with('fail', '类别创建失败');
        }

    }

    public function edit($id)
    {
        try {
            $category = Category::find($id);
        } catch (\Exception $e) {
            return redirect('admin.categories.index')->with('fail', '未找到该类别');
        }
        return view('admin.categories.edit', compact('category'));
    }

    public function update($id, Request $request)
    {
        $data_array = $request->all();
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $img_path = $request->file('image')->store('categories/images');
            } else {
                return redirect(route('admin.categories.index'))->with('fail', '图片无效');
            }
        }
        if (isset($img_path)) {
            $data_array['image'] = $img_path;
        }
        try {
            $category = Category::find($id);
            if ($category->image != null) {
                Storage::delete($category->image);
            }
            $category->update($data_array);
            return redirect(route('admin.categories.edit', ['id' => $id]))->with('success', '更新成功');
        } catch (\Exception $e) {
            return redirect(route('admin.categories.edit', ['id' => $id]))->with('fail', '更新失败');
        }
    }

    public function delete($id)
    {
        try {
            $category = Category::find($id);
        } catch (\Exception $e) {
            return redirect(route('admin.categories.index'))->with('fail', '未找到类别');
        }

        try {
            $category->delete();
            return redirect(route('admin.categories.index'))->with('success', '删除类别成功');
        } catch (\Exception $e) {
            return redirect(route('admin.categories.index'))->with('fail', '删除类别失败');
        }
    }

    public function trashed(){
        $categories = Category::onlyTrashed()->get();
        return view('admin.categories.trashed',compact('categories'));
    }

    public function restore($id){
        $category = Category::onlyTrashed()->find($id);
        $category->restore();
        return redirect(route('admin.categories.index'))->with('success','恢复成功');
    }
}
