<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Subcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function index(){
        $products = Product::all();
        return view('admin.products.index',compact('products'));
    }

    public function show($id){

    }

    public function create(){
        $categories = Category::all();
        $category_options = array();
        foreach($categories as $category){
            $category_options[] = array('value'=>$category->id,'text'=>$category->name);
        }
        $subcategories = Subcategory::all();
        $subcategory_options = array();
        foreach($subcategories as $subcategory){
            $subcategory_options[] = array('value'=>$subcategory->id,'text'=>$subcategory->name);
        }
        return view('admin.products.create',compact('category_options','subcategory_options'));
    }

    public function store(Request $request){
        $data_array = $request->all();
        if($request->hasFile('image')){
            if($request->file('image')->isValid()){
                try{
                    $img_path = $request->file('image')->store('products');
                } catch (\Exception $e){
                    Log::debug($e);
                    return redirect(route('admin.products.create'))->with('fail','图片上传错误')->withInput();
                }
            } else {
                return redirect(route('admin.products.create'))->with('fail','图片上传错误')->withInput();
            }
        }
        if(isset($img_path)){
            $data_array['image'] = $img_path;
        }
        try{
            Product::create($data_array);
            return redirect(route('admin.products.index'))->with('success','产品上传成功');
        } catch(\Exception $e) {
            Log::debug($e);
            return redirect(route('admin.products.index'))->with('success','产品创建失败');
        }

    }

    public function edit($id){
        try {
            $product = Product::withTrashed()->find($id);
        } catch (\Exception $e){
            return redirect(route('admin.products.index'))->with('fail','未找到产品');
        }
        $categories = Category::all();
        $category_options = array();
        foreach ($categories as $category){
            $category_options[] = array('value'=>$category->id,'text'=>$category->name);
        }
        $subcategories = Subcategory::all();
        $subcategory_options = array();
        foreach($subcategories as $subcategory){
            $subcategory_options[] = array('value'=>$subcategory->id,'text'=>$subcategory->name);
        }
        return view('admin.products.edit',compact('product','category_options','subcategory_options'));
    }

    public function update($id, Request $request){
        $data_array = $request->all();
        try{
            $product = Product::withTrashed()->find($id);
        } catch(\Exception $e){
            return redirect(route('admin.products.index'))->with('fail','未找到产品');
        }

        if($request->hasFile('image')){
            if($request->file('image')->isValid()){
                try{
                    $img_path = $request->file('image')->store('products');
                } catch (\Exception $e){
                    Log::debug($e);
                    return redirect(route('admin.products.create'))->with('fail','图片上传错误')->withInput();
                }
            } else {
                return redirect(route('admin.products.create'))->with('fail','图片上传错误')->withInput();
            }
        }
        if(isset($img_path)){
            $data_array['image'] = $img_path;
            try{
                Storage::delete($product->image);
            } catch(\Exception $e){
                Log::debug("***** Couldn't delete old product image *****");
                Log::debug($e);
            }
        }
        try{
            $product->update($data_array);
            return redirect(route('admin.products.edit',['id'=>$product->id]))->with('success','产品更新成功');
        } catch(\Exception $e) {
            Log::debug($e);
            return redirect(route('admin.products.index'))->with('fail','产品更新失败');
        }
    }

    public function delete($id){
        try{
            $product = Product::find($id);
        } catch (\Exception $e){
            return redirect(route('admin.products.index'))->with('fail','未找到产品');
        }
        try{
            $product->delete();
            return redirect(route('admin.products.index'))->with('success','产品删除成功');
        } catch (\Exception $e){
            Log::debug($e);
            return redirect(route('admin.products.index'))->with('fail','删除失败');
        }
    }

    public function trashed(){
        $products = Product::onlyTrashed()->get();
        return view('admin.products.trashed',compact('products'));
    }

    public function restore($id){
        Product::onlyTrashed()->find($id)->restore();
        return redirect(route('admin.products.trashed'))->with('success','恢复产品成功');
    }
}
