<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Subcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class SubcategoryController extends Controller
{
    public function index(){
        $subcategories = Subcategory::all();
        return view('admin.subcategories.index',compact('subcategories'));
    }

    public function show($id){

    }

    public function create(){
        $categories = Category::all();
        $category_options = array();
        foreach ($categories as $category) {
            $category_options[] = array('value'=>$category->id,'text'=>$category->name);
        }
        return view('admin.subcategories.create',compact('category_options'));
    }

    public function store(Request $request){
        try{
            Subcategory::create($request->all());
            return redirect(route('admin.subcategories.index'))->with('success','创建子类别成功');
        } catch(\Exception $e){
            return redirect(route('admin.subcategories.create'))->withInput();
        }
    }

    public function edit($id){
        $subcategory = Subcategory::find($id);
        $categories = Category::all();
        $category_options = array();
        foreach($categories as $category){
            $category_options[] = array('value'=>$category->id,'text'=>$category->name);
        }
        return view('admin.subcategories.edit',compact('subcategory','category_options'));
    }

    public function update($id, Request $request){
        $subcategory = Subcategory::find($id);
        try{
            $subcategory->update($request->all());
            return redirect(route('admin.subcategories.index'))->with('success','更新子类别成功');
        } catch(\Exception $e){
            Log::debug($e);
            return redirect(route('admin.subcategories.edit',['id'=>$id]))->with('fail','更新子类别失败');
        }
    }

    public function delete($id){
        $subcategory = Subcategory::find($id);
        try{
            $subcategory->delete();
            return redirect(route('admin.subcategories.index'))->with('success','删除成功');
        } catch(\Exception $e){
            Log::debug($e);
            return redirect(route('admin.subcategories.index'))->with('fail','删除失败');
        }
    }
}
