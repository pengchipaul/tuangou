<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function login(){
        return view('admin.users.login');
    }

    public function index(){
        $users = User::all();
        return view('admin.users.index',compact('users'));
    }

    public function show($id){

    }

    public function create(){
        return view('admin.users.create');
    }

    public function store(Request $request){
        try{
            $data_array = $request->all();
            $data_array['email'] = $data_array['name'];
            $data_array['password'] = Hash::make($data_array['password']);
            User::create($data_array);
            return redirect(route('admin.users.index'))->with('success','用户创建成功');
        } catch(\Exception $e){
            Log::debug($e);
            return redirect(route('admin.users.create'))->with('fail','用户创建失败')->withInput();
        }
    }

    public function edit($id){

    }

    public function update($id, Request $request){

    }

    public function delete($id){

    }

    public function staff_index(){
        $admins = User::where('admin',true)->get();
        $staffs = User::where('staff','!=',0)->get();
        $users = $admins->merge($staffs);
        return view('admin.users.index',compact('users'));
    }
}
