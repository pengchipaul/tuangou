<?php

namespace App\Http\Controllers\Custom;

use App\Coupon;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;

class AdminController extends Controller
{
    /**
     * Generating coupon functions
     * @param $digits, number of digits of a coupon
     * @param $value, the value of coupon
     * @param $number, number of coupons that needs to be generated
     */
    public static function generate_coupon($digits, $value, $number){
        /*
         * Data Validation
         */
        if($digits > 10 || $digits < 6 || $value <= 0 || $number <= 0){
            return "Invalid Parameters";
        }

        /*
         * Generate Random String and Create Coupon
         */
        $total = 0;
        while($total != $number){
            $code = strtoupper(str_random($digits));
            while(strpos($code,"0") !== false || strpos($code,"O") !== false || strpos($code,"U") !== false){
                $code = strtoupper(str_random($digits));
            }
            try{
                Coupon::create(['code'=>$code,'value'=>$value]);
                $total++;
            } catch (\Exception $e){
                Log::debug($e);
            }
        }
        return "completed";
    }

    public static function stocks_and_sales(){
        $products = Product::all();
        foreach($products as $product){
            $stock = rand(25,35);
            $sales = rand(15,25);
            $product->stock = $stock;
            $product->sales = $sales;
            $product->save();
        }
        return "completed";
    }


}
