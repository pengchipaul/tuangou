<?php

namespace App\Http\Controllers\Api;

use App\Order;
use App\OrderProduct;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{
    /**
     * @param Request $request, includeing shopping cart info and order info
     */
    public function create(Request $request){

        // initialize empty OrderProduct array
        $order_products = array();

        // validate shopping cart info
        try{
            $shopping_cart = $request->get('cart');
            $total = 0;
            foreach($shopping_cart as $cart_item){
                $order_products[] = array('product_id'=>$cart_item['product_id'],'quantity'=>$cart_item['quantity']);
                $total += Product::find($cart_item['product_id'])->price;
            }
        } catch(\Exception $e){
            Log::debug($e);
            return response()->json(['message'=>'error','data'=>'购物车获取失败'],200);
        }

        // validate order info
        if(!$request->has('phone') || !$request->has('payment_method')){
            return response()->json(['message'=>'error','data'=>'订单信息格式错误'],200);
        }
        $payment_method = $request->payment_method;
        if($payment_method != 'wechat' && $payment_method != 'later'){
            return response()->json(['message'=>'error','data'=>'订单支付方式错误'],200);
        }

        // attempt to create order info
        try{
            $order = Order::create(['phone'=>$request->get('phone'),'payment_method'=>$request->get('payment_method'),'total'=>$total]);
        } catch(\Exception $e){
            Log::debug($e);
            return response()->json(['message'=>'error','data'=>'订单创建失败'],200);
        }

        // attempt to create order product
        try{
            foreach($order_products as $order_product){
                OrderProduct::create(['product_id'=>$order_product['product_id'],'order_id'=>$order->id,'quantity'=>$order_product['quantity']]);
            }
            return $this->create_payment($order);
        } catch(\Exception $e){
            Log::debug($e);
            OrderProduct::where('order_id',$order->id)->delete();
            $order->delete();
            return response()->json(['message'=>'error','data'=>'订单商品创建失败'],200);
        }
    }

    public function create_payment($order){
        return response()->json(['message'=>'success','data'=>'http://tuangou.tangqi.com.au'],200);
    }
}
