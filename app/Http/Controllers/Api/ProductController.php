<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;

class ProductController extends Controller
{
    public function index(){
        $products = Product::all();
        return response()->json(['message'=>'success','data'=>$products],200);
    }
}
