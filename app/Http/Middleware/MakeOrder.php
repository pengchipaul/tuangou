<?php

namespace App\Http\Middleware;

use Closure;

class MakeOrder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!session()->has('cart')){
            return redirect(route('users.show_cart'));
        }
        $cart = session()->get('cart');
        if(count($cart) == 0){
            return redirect(route('users.show_cart'));
        }
        return $next($request);
    }
}
