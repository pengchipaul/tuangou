<?php

namespace App\Http\Middleware;

use Closure;

class CompleteOrderInfo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!session()->has('order_info')){
            return redirect(route('users.orders.show_form'));
        }
        return $next($request);
    }
}
