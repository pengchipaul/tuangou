<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\ApiKey;
use Illuminate\Support\Facades\Log;


class BoxhillApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->isMethod('post')){
            if(!$request->has('api_id') || !$request->has('api_key')){
                return response()->json(['message' => 'Invalid request format'],200);
            }
            try{
                $api_key = ApiKey::find($request->get('api_id'));
                if($api_key->name == "boxhill" && $api_key->key == $request->get('api_key')){
                    return $next($request);
                } else {
                    return response()->json(['message' => 'Invalid key and id'],200);
                }
            } catch(\Exception $e){
                return response()->json(['message' => 'Invalid key and id'],200);
            }

        }
        if($request->isMethod('get')){
            Log::debug($request->header('Store_KEY'));
            if(!$request->hasHeader('Store_KEY') || !$request->hasHeader('Store_ID')){
                return response()->json(['message' => 'Invalid request format'],200);
            }

            try{
                $api_key = ApiKey::find($request->header('Store_ID'));
                if($api_key->name == "boxhill" && $api_key->key == $request->header('Store_KEY')){
                    return $next($request);
                } else {
                    return response()->json(['message' => 'Invalid key and id'],200);
                }
            } catch(\Exception $e){
                return response()->json(['message' => 'Invalid key and id'],200);
            }
        }
        return response()->json(['message' => 'Invalid request'],200);
    }
}
