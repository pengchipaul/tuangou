<?php

namespace App\Jobs;

use App\Notifications\ReportBug;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Notification;


class BugNotify implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $action, $error;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($error, $action)
    {
        $this->error = $error;
        $this->action = $action;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Notification::route('mail','paulpengwork@gmail.com')->notify(new ReportBug(strval($this->action).strval($this->error)));
    }
}
