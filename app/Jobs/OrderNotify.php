<?php

namespace App\Jobs;

use App\Order;
use App\Notifications\NewOrder;
use App\OrderProduct;
use Illuminate\Support\Facades\Log;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Notification;

class OrderNotify implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $order;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $order = $this->order;

        /* send sms to customer */
        $aus_num = ltrim($order->phone,"0");
        $aus_num = "61".$aus_num;
        $save = false;
        while($save == false){
            try{
                $code = strtoupper(str_random(6));
                $order->code = $code;
                $order->save();
                $save = true;
            } catch (\Exception $e){
                Log::debug($e);
            }
        }


        try{
            Notification::route('nexmo',$aus_num)->notify(new NewOrder($order->id));
        } catch (\Exception $e){
            Log::debug($e);
            BugNotify::dispatch(strval($e),"sending sms to customer mobile phone ".$aus_num);
        }

        /* send email to staff/admin */
        try{
            Notification::route('mail','sales@tangqi.com.au')->notify(new NewOrder($order->id));
            Notification::route('mail','paulpengwork@gmail.com')->notify(new NewOrder($order->id));
        } catch(\Exception $e){
            Log::debug($e);
        }

    }
}
