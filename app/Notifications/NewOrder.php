<?php

namespace App\Notifications;

use App\Order;
use App\Coupon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\NexmoMessage;

class NewOrder extends Notification implements ShouldQueue
{
    use Queueable;

    protected $order_id;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order_id)
    {
        $this->order_id = $order_id;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['nexmo','mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $order = Order::find($this->order_id);
        $order_num = sprintf("%08d",$order->id);
        $pickup_time  = $order->pickup_time;
        $location = $pickup_time->location;
        $location->detail = str_replace('\n',',',$location->detail);
        $line1 = "新订单！"."用户名:".$order->name.",用户手机:".$order->phone;
        $line2 = "订单提交成功,订单号:".$order_num."; 总金额为:$".$order->total."; 提货验证码为 ".$order->code.". ";
        $line3 = "提货点:".$location->name.",".$location->detail;
        $line4 = "提货时间:".$pickup_time->time;

        return (new MailMessage)
                    ->from("tuanduoduo@tangqi.com.au")
                    ->line($line1)
                    ->line($line2)
                    ->line($line3)
                    ->line($line4);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toNexmo($notifiable){
        /* get order information */
        $order = Order::find($this->order_id);
        $pickup_time = $order->pickup_time;
        $location = $pickup_time->location;
        $location->detail = str_replace('\n',',',$location->detail);
        $order_num = sprintf("%08d",$order->id);

        /* generate coupon */
        if($order->status == 1){
            $total = 0;
            $digits = 6;
            $value = number_format($order->total / 10,2,'.','');
            // generate one coupon
            while($total != 1){
                $code = strtoupper(str_random($digits));
                while(strpos($code,"0") !== false || strpos($code,"O") !== false || strpos($code,"U") !== false){
                    $code = strtoupper(str_random($digits));
                }
                try{
                    $coupon = Coupon::create(['code'=>$code,'value'=>$value]);
                    $total++;
                } catch (\Exception $e){
                    Log::debug($e);
                }
            }
        }

        if($order->lang == "en"){
            $content = "Hello ".$order->name.",\n";
            $content .= "Thank you for shopping at Crazyget of First Shopping!\n\n";
            $content .= "Order Number: ".$order_num."\n";
            $content .= "Amount: $".$order->total."\n";
            $content .= "Pick-up Verification Code: ".$order->code."\n";
            $content .= "Pick-up Location: ".__($location->name).",".$location->detail."\n";
            $content .= "Pick-up Time: ".__($pickup_time->time)."\n";
            $content .= "Please save this message for pick-up verification.\n";
            if($order->is_store == false){
                $content .= "Your order will be automatically delivered on the next pick-up date if you miss your pick-up time of the day.\n";
            }
            $content .= "We look forward to seeing you again. Have a great day!\n";
            $content .= "Best Regards,\n";
            $content .= "First Shopping Group";
            return (new NexmoMessage)
                ->content($content)
                ->unicode();
        }

        $content = "恭喜您在First Shopping壹商店团多多下单成功！\n";
        $content .= "订单号: ".$order_num."\n";
        $content .= "消费金额: $".$order->total."\n";
        if(isset($coupon)){
            $content .= "代金券: $".$coupon->value."\n";
            $content .= "代金劵验证码: ".$coupon->code."\n";
        }
        $content .= "您的提货验证码:".$order->code."\n";
        $content .= "提货地点: ".$location->name."，".$location->detail."\n";
        $content .= "提货时间: ".$pickup_time->time."\n";
        $content .= "请保存好此信息，用于提货验证。";
        if($order->is_store == false){
            $content .= "如错过当天提货时间，您的订单会自动顺延到下一个提货日。";
        }
        $content .= "非常感谢您的惠顾！";

        return (new NexmoMessage)
            ->content($content)
            ->unicode();
    }
}
