<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\NexmoMessage;

use App\Coupon;
use App\Order;

class SendCoupon extends Notification implements ShouldQueue
{
    use Queueable;

    protected $order_id;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order_id)
    {
        $this->order_id = $order_id;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['nexmo'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toNexmo($notifiable){
        $order = Order::find($this->order_id);
        $total = 0;
        $digits = 6;
        $value = $order->total / 10;
        // generate one coupon
        while($total != 1){
            $code = strtoupper(str_random($digits));
            while(strpos($code,"0") !== false || strpos($code,"O") !== false || strpos($code,"U") !== false){
                $code = strtoupper(str_random($digits));
            }
            try{
                $coupon = Coupon::create(['code'=>$code,'value'=>$value]);
                $total++;
            } catch (\Exception $e){
                Log::debug($e);
            }
        }
        $content = "代金券: $".$coupon->value."\n";
        $content .= "代金劵验证码: ".$coupon->code;

        return (new NexmoMessage)
            ->content($content)
            ->unicode();
    }
}
