<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = ['name','detail'];

    public function orders(){
        return $this->hasManyThrough('App\Order','App\PickupTime');
    }

    public function pickup_times(){
        return $this->hasMany('App\PickupTime');
    }
}
