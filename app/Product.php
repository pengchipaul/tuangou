<?php

namespace App;

use App\OrderProduct;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable = ['name','RRP','price','deadline','sales','real_sales','stock','category_id','image','subcategory_id','barcode'];

    protected $dates = ['deleted_at'];

    protected $hidden = ['real_sales','deleted_at'];

    public function category(){
        return $this->belongsTo('App\Category')->withTrashed();
    }

    public function subcategory(){
        return $this->belongsTo('App\Subcategory')->withTrashed();
    }

    public function get_quantity($order_id){
        try{
            return OrderProduct::where(['order_id'=>$order_id,'product_id'=>$this->id])->first()->quantity;
        } catch (\Exception $e){
            Log::debug($e);
            return null;
        }
    }
}
