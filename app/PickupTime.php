<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PickupTime extends Model
{
    protected $fillable = ['time','location_id'];

    public function location(){
        return $this->belongsTo('App\Location');
    }
}
