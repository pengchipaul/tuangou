<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['name','phone','payment_method','total','payment_link','pickup_time_id','lang'];

    public function products(){
        return $this->belongsToMany('App\Product','order_products')->withTrashed();
    }

    public function order_products(){
        return $this->hasMany('App\OrderProduct');
    }

    public function pickup_time(){
        return $this->belongsTo('App\PickupTime');
    }
}
